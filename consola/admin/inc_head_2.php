<!-- BEGIN THEME STYLES -->
<link href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/css/netgocio.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="<?php echo NOME_SITE; ?>"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="<?php echo ROOTPATH_HTTP; ?>imgs/favicon/mstile-310x310.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->