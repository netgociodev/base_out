
<div class="page-header -i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>admin/index.php">
			<img src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>imgs/logo.png" alt="logo" class="logo-default" style="max-width: 100px;"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<?php if($consolaLG_count > 1) { ?>
					<li class="dropdown dropdown-user">
						<a href="javascript:;" class="dropdown-toggle dropdown-toggle-linguas flag-linguas" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="background-image: url('<?php echo ROOTPATH_HTTP_CONSOLA;?>imgs/linguas/<?php echo $row_rsUser['lingua'];?>.png');" title="<?php echo $RecursosCons->RecursosCons['selec_idioma_back_txt']; ?>"> <?php echo $row_rsUser['lingua'];?> <i id="angle-down-linguas" class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default dropdown-linguas">
							<?php foreach ($row_rsconsolaLG as $value) { ?>
								<li class="li-linguas" id="li-<?php echo $value['sufixo']; ?>">
									<a href="javascript:void(null)" onClick="changeLG_login('<?php echo $value['sufixo']; ?>');" class="flag-linguas" style="background-image: url('<?php echo ROOTPATH_HTTP_CONSOLA;?>imgs/linguas/<?php echo $value['sufixo']; ?>.png');"> <?php echo $value['sufixo']; ?></a>
								</li> 
							<?php } ?>
						</ul>
					</li>
				<?php } ?><li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" class="img-circle" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>imgs/user/<?php echo $row_rsUser["imagem1"]; ?>" width="29"/>
					<span class="username username-hide-on-mobile">
					<?php echo $nome_mostra; ?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="<?php echo ROOTPATH_HTTP_ADMIN; ?>user/perfil-alterar.php">
							<i class="icon-user"></i> <?php echo $RecursosCons->RecursosCons['alterar_perfil_menu']; ?></a>
						</li>
						<li class="divider">
						</li>                        
						<li>
							<a href="javascript:void(null)" onClick="logout(2, '<?php echo ROOTPATH_HTTP_CONSOLA; ?>')">
							<i class="icon-lock"></i> <?php echo $RecursosCons->RecursosCons['bloquar_sessao_menu']; ?> </a>
						</li>
						<li>
							<a href="javascript:void(null)" onClick="logout(1, '<?php echo ROOTPATH_HTTP_CONSOLA; ?>')">
							<i class="icon-key"></i> <?php echo $RecursosCons->RecursosCons['terminar_sessao_menu']; ?> </a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<!--<li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="javascript:;" class="dropdown-toggle">
					<i class="icon-logout"></i>
					</a>
				</li>-->
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->