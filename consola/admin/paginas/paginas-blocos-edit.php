<?php include_once('../inc_pages.php'); ?>
<?php

$id = $_GET['id'];
$fixo = $_GET['fixo'];
$pagina = $_GET['pagina'];

$menu_sel='paginas';
$menu_sub_sel='paginas_fixas';
$nome_sel='P�ginas Fixas';

if($fixo == 0) {
  $menu_sub_sel='paginas_outras';
  $nome_sel='Outras P�ginas';
}

$erro_insert = 0;

$tamanho_imagens1 = getFillSize('Paginas', 'imagem4');

$tab_sel = 1;
if(isset($_REQUEST['tab_sel']) && $_REQUEST['tab_sel'] != "" && $_REQUEST['tab_sel'] != 0) $tab_sel=$_REQUEST['tab_sel'];

if((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "paginas_blocos_form")) {

  $query_rsP = "SELECT * FROM paginas_blocos".$extensao." WHERE id=:id";
  $rsP = DB::getInstance()->prepare($query_rsP);
  $rsP->bindParam(':id', $id, PDO::PARAM_INT, 5);
  $rsP->execute();
  $row_rsP = $rsP->fetch(PDO::FETCH_ASSOC);
  $totalRows_rsP = $rsP->rowCount();

  if($_POST['nome']!='' && $tab_sel == 1) {
    $insertSQL = "SELECT MAX(id) FROM paginas_blocos_$lingua_consola";
    $rsInsert = DB::getInstance()->prepare($insertSQL);
    $rsInsert->execute();
    $row_rsInsert = $rsInsert->fetch(PDO::FETCH_ASSOC);
    DB::close();
    
    $max_id = $row_rsInsert["MAX(id)"]+1;
        
    $insertSQL = "UPDATE paginas_blocos".$extensao." SET nome=:nome, titulo=:titulo, titulo1=:titulo1, texto=:texto, titulo2=:titulo2, texto2=:texto2, titulo3=:titulo3, texto3=:texto3, link1=:link1, texto_botao1=:texto_botao1, link2=:link2, texto_botao2=:texto_botao2, link3=:link3, texto_botao3=:texto_botao3, mapa=:mapa WHERE id=:id";
    $rsInsert = DB::getInstance()->prepare($insertSQL);
    $rsInsert->bindParam(':nome', $_POST['nome'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':titulo', $_POST['titulo'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':titulo1', $_POST['titulo1'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':texto', $_POST['texto'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':titulo2', $_POST['titulo2'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':texto2', $_POST['texto2'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':titulo3', $_POST['titulo3'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':texto3', $_POST['texto3'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':link1', $_POST['link1'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':texto_botao1', $_POST['texto_botao1'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':link2', $_POST['link2'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':texto_botao2', $_POST['texto_botao2'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':link3', $_POST['link3'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':texto_botao3', $_POST['texto_botao3'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':mapa', $_POST['mapa'], PDO::PARAM_STR, 5);
    $rsInsert->bindParam(':id', $id, PDO::PARAM_INT);
    $rsInsert->execute();

    $tipo_galeria = 0;
    if(isset($_POST['tipo_galeria']))
      $tipo_galeria = 1;

    $fullscreen = 0;
    if(isset($_POST['fullscreen']))
      $fullscreen = 1;

    $tipo_imagens = 0;
    if(isset($_POST['tipo_imagens']))
      $tipo_imagens = 1;

    $query_rsLinguas = "SELECT sufixo FROM linguas WHERE visivel = '1'";
    $rsLinguas = DB::getInstance()->prepare($query_rsLinguas);
    $rsLinguas->execute();
    $totalRows_rsLinguas = $rsLinguas->rowCount();

    $contorna = 0;
    if($_POST['orientacao'] == 0 || $_POST['orientacao'] == 1){
      $contorna = $_POST['contorna'];
    }

    while($row_rsLinguas = $rsLinguas->fetch()) {
      $insertSQL = "UPDATE paginas_blocos_".$row_rsLinguas['sufixo']." SET target1=:target1, target2=:target2, target3=:target3, orientacao=:orientacao, colunas=:colunas, texto_contorna=:contorna, tipo_imagens=:tipo_imagens, esp_imagens=:esp_imagens, fullscreen=:fullscreen, tipo_galeria=:tipo_galeria, video=:video, largura_texto=:largura_texto, valor_largura_texto=:valor_largura_texto WHERE id =:id";
      $rsInsert = DB::getInstance()->prepare($insertSQL);
      $rsInsert->bindParam(':target1', $_POST['target1'], PDO::PARAM_STR, 5);
      $rsInsert->bindParam(':target2', $_POST['target2'], PDO::PARAM_STR, 5);
      $rsInsert->bindParam(':target3', $_POST['target3'], PDO::PARAM_STR, 5);
      $rsInsert->bindParam(':orientacao', $_POST['orientacao'], PDO::PARAM_INT);
      $rsInsert->bindParam(':contorna', $contorna, PDO::PARAM_INT);
      $rsInsert->bindParam(':colunas', $_POST['colunas'], PDO::PARAM_INT);
      $rsInsert->bindParam(':tipo_imagens', $tipo_imagens, PDO::PARAM_INT);
      $rsInsert->bindParam(':esp_imagens', $_POST['esp_imagens'], PDO::PARAM_INT);
      $rsInsert->bindParam(':tipo_galeria', $tipo_galeria, PDO::PARAM_INT);
      $rsInsert->bindParam(':fullscreen', $fullscreen, PDO::PARAM_INT);
      $rsInsert->bindParam(':video', $_POST['video'], PDO::PARAM_STR, 5);
      $rsInsert->bindParam(':largura_texto', $_POST['largura_texto'], PDO::PARAM_INT);
      $rsInsert->bindParam(':valor_largura_texto', $_POST['valor_largura_texto'], PDO::PARAM_INT);
      $rsInsert->bindParam(':id', $id, PDO::PARAM_INT);
      $rsInsert->execute();
    }

    alteraSessions('paginas');
    alteraSessions('paginas_menu');
    alteraSessions('paginas_fixas');
    
    $inserido = 1;
  }

  if($tab_sel == 3){

    $tem_fundo=0;
    if(isset($_POST['tem_fundo'])) $tem_fundo=1;

    $tipo_fundo = $_POST['tipo_fundo'];

    $mascara = 0;
    if(isset($_POST['mascara_fundo'])) {
      $mascara = 1;
    }

    $query_rsLinguas = "SELECT * FROM linguas WHERE visivel = 1";
    $rsLinguas = DB::getInstance()->prepare($query_rsLinguas);
    $rsLinguas->execute();
    DB::close();

    while($row_rsLinguas = $rsLinguas->fetch()) {
      $insertSQL = "UPDATE paginas_blocos_".$row_rsLinguas['sufixo']." SET tem_fundo=:tem_fundo, tipo_fundo=:tipo_fundo, cor_fundo=:cor_fundo, mascara_fundo=:mascara_fundo WHERE id=:id";
      $rsInsert = DB::getInstance()->prepare($insertSQL);
      $rsInsert->bindParam(':tem_fundo', $tem_fundo, PDO::PARAM_INT); 
      $rsInsert->bindParam(':tipo_fundo', $tipo_fundo, PDO::PARAM_INT);
      $rsInsert->bindParam(':cor_fundo', $_POST['cor_fundo'], PDO::PARAM_STR, 5);
      $rsInsert->bindParam(':mascara_fundo', $mascara, PDO::PARAM_INT);
      $rsInsert->bindParam(':id', $id, PDO::PARAM_INT, 5);  
      $rsInsert->execute();
    }


    $imagem = $row_rsP['imagem_fundo'];

    if(isset($_POST['img_remover1']) && $_POST['img_remover1']==1) {
      $insertSQL = "UPDATE paginas_blocos".$extensao." SET imagem_fundo=NULL WHERE id=:id";
      $rsInsert = DB::getInstance()->prepare($insertSQL);
      $rsInsert->bindParam(':id', $id, PDO::PARAM_INT, 5);  
      $rsInsert->execute();

      @unlink('../../../imgs/paginas/'.$imagem);
    }

    if($_FILES['img']['name']!='') { // actualiza imagem
      //Verificar o formato do ficheiro
      $ext = strtolower(pathinfo($_FILES['img']['name'], PATHINFO_EXTENSION));

      if($ext != "jpg" && $ext != "jpeg" && $ext != "gif" && $ext != "png") {
        $erro = 1;
      }
      else {
        $ins = 1; 
        require("../resize_image.php");
        
        $imagem=""; 
        
        $imgs_dir = "../../../imgs/paginas";
        $contaimg = 1; 
    
        foreach($_FILES as $file_name => $file_array) {
      
          $id_file=date("his").'_'.$contaimg.'_'.rand(0,9999);
          
          switch ($contaimg) {
            case '1': case '2': case '3':    
              $file_dir =  $imgs_dir;
            break;
          }
          
    
          if($file_array['size'] > 0){
              $nome_img=verifica_nome($file_array['name']);
              $nome_file = $id_file."_".$nome_img;
              @unlink($file_dir.'/'.$_POST['file_db_'.$contaimg]);
          }else {
              //$nome_file = $_POST['file_db_'.$contaimg];
    
            if($_POST['file_db_'.$contaimg])
              $nome_file = $_POST['file_db_'.$contaimg];
            else{
              $nome_file ='';
              @unlink($file_dir.'/'.$_POST['file_db_del_'.$contaimg]);
              }
    
          }
              
          if (is_uploaded_file($file_array['tmp_name'])) { move_uploaded_file($file_array['tmp_name'],"$file_dir/$nome_file") or die ("Couldn't copy"); }
    
          //store the name plus index as a string 
          $variableName = 'nome_file' . $contaimg; 
          //the double dollar sign is saying assign $imageName 
          // to the variable that has the name that is in $variableName
          $$variableName = $nome_file;  
          $contaimg++;
                              
        } // fim foreach
        //Fim do Trat. Imagens
          
        //RESIZE DAS IMAGENS
        $imagem = $nome_file1;

        //IMAGEM 1
        if($_FILES['img']['name']!='') {
          if($imagem!="" && file_exists("../../../imgs/paginas/".$imagem)){
                    
            $maxW=$tamanho_imagens1['0'];
            $maxH=$tamanho_imagens1['1'];
            
            $sizes=getimagesize("../../../imgs/paginas/".$imagem);
            
            $imageW=$sizes[0];
            $imageH=$sizes[1];
            
            if($imageW>$maxW || $imageH>$maxH){
                      
              $img1=new Resize("../../../imgs/paginas/", $imagem, $imagem, $maxW, $maxH);
              $img1->resize_image();
              
            }
          }   
          
          if($row_rsP['imagem_fundo']){
            @unlink('../../../imgs/paginas/'.$row_rsP['imagem_fundo']);
          }

          compressImage('../../../imgs/paginas/'.$imagem, '../../../imgs/paginas/'.$imagem);

          $insertSQL = "UPDATE paginas_blocos".$extensao." SET imagem_fundo=:imagem_fundo WHERE id=:id";
          $rsInsert = DB::getInstance()->prepare($insertSQL);
          $rsInsert->bindParam(':imagem_fundo', $imagem, PDO::PARAM_STR, 5);
          $rsInsert->bindParam(':id', $id, PDO::PARAM_INT, 5);    
          $rsInsert->execute();
        }
      }
    }

    DB::close();

    alteraSessions('paginas');
    alteraSessions('paginas_menu');
    alteraSessions('paginas_fixas');

    header("Location: paginas-blocos-edit.php?id=".$id."&pagina=".$pagina."&fixo=".$fixo."&alt=1&tab_sel=3");    
  }
}

$query_rsP = "SELECT * FROM paginas".$extensao." WHERE id=:id";
$rsP = DB::getInstance()->prepare($query_rsP);
$rsP->bindParam(':id', $pagina, PDO::PARAM_INT, 5); 
$rsP->execute();
$row_rsP = $rsP->fetch(PDO::FETCH_ASSOC);
$totalRows_rsP = $rsP->rowCount();

$query_rsP2 = "SELECT * FROM paginas_blocos".$extensao." WHERE id=:id";
$rsP2 = DB::getInstance()->prepare($query_rsP2);
$rsP2->bindParam(':id', $id, PDO::PARAM_INT, 5);  
$rsP2->execute();
$row_rsP2 = $rsP2->fetch(PDO::FETCH_ASSOC);
$totalRows_rsP2 = $rsP2->rowCount();

DB::close();

?>
<?php include_once(ROOTPATH_ADMIN.'inc_head_1.php'); ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/fancybox/jquery.fancybox.min.css"/>
<style type="text/css">
  #texto_div, #texto2_div, #texto3_div {
    display: none;
  }
</style>
<!-- END PAGE LEVEL STYLES -->
<?php include_once(ROOTPATH_ADMIN.'inc_head_2.php'); ?>
<body class="<?php echo $body_info; ?>">
<?php include_once(ROOTPATH_ADMIN.'inc_topo.php'); ?>
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
  <?php include_once(ROOTPATH_ADMIN.'inc_menu.php'); ?>
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <div class="page-content"> 
      <!-- BEGIN PAGE HEADER-->
      <h3 class="page-title"> <?php echo $RecursosCons->RecursosCons['paginas']; ?> <small><?php echo $nome_sel; ?> - <?php echo $RecursosCons->RecursosCons['blocos']; ?> </small> </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li> <i class="fa fa-home"></i> <a href="../index.php"><?php echo $RecursosCons->RecursosCons['home']; ?></a> <i class="fa fa-angle-right"></i> </li>
            <li>
                <a href="paginas.php?fixo=<?php echo $fixo; ?>"><?php echo $RecursosCons->RecursosCons['paginas']; ?></a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="paginas.php?fixo=<?php echo $fixo; ?>&id=<?php echo $pagina; ?>"><?php echo $RecursosCons->RecursosCons['blocos']; ?></a>
            </li>
        </ul>
      </div>
      <!-- END PAGE HEADER--> 
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
          <?php include_once(ROOTPATH_ADMIN.'inc_linguas.php'); ?> 
          <form id="paginas_blocos_form" name="paginas_blocos_form" class="form-horizontal form-row-seperated" method="post" role="form" enctype="multipart/form-data">
            <input type="hidden" name="tab_sel" id="tab_sel" value="<?php echo $tab_sel; ?>">
            <input type="hidden" name="img_remover1" id="img_remover1" value="0">
            <div class="portlet">
              <div class="portlet-title">
                <div class="caption"> <i class="fa fa-pencil-square"></i><?php echo $nome_sel; ?> - <?php echo $row_rsP['nome']; ?> - <?php echo $RecursosCons->RecursosCons['bloco']; ?> - <?php echo $row_rsP2['nome']; ?></div>
                <div class="form-actions actions btn-set">
                  <button type="button" name="back" class="btn default" onClick="document.location='paginas-blocos.php?pagina=<?php echo $pagina; ?>&fixo=<?php echo $fixo; ?>'"><i class="fa fa-angle-left"></i> <?php echo $RecursosCons->RecursosCons['voltar']; ?></button>
                  <button type="reset" class="btn default"><i class="fa fa-eraser"></i> <?php echo $RecursosCons->RecursosCons['limpar']; ?></button>
                  <button type="submit" class="btn green"><i class="fa fa-check"></i> <?php echo $RecursosCons->RecursosCons['guardar']; ?></button>
                </div>
              </div>
              <div class="portlet-body">
                <ul class="nav nav-tabs">
                  <li <?php if($tab_sel==1) echo "class=\"active\""; ?> ><a href="#tab_detalhes" data-toggle="tab" onClick="document.getElementById('tab_sel').value='1'"> <?php echo $RecursosCons->RecursosCons['tab_detalhes']; ?> </a> </li>
                  <?php if($row_rsP2['tipo'] == 1 || $row_rsP2['tipo'] == 1 || $row_rsP2['tipo'] == 3){ ?>
                    <li onclick="window.location='paginas-blocos-imagens.php?id=<?php echo $id; ?>&pagina=<?php echo $pagina; ?>&fixo=<?php echo $fixo; ?>'"><a href="javascript:void(null)"> <?php echo $RecursosCons->RecursosCons['tab_imagens_videos']; ?> </a> </li>
                  <?php } 
                  if($row_rsP2['tipo'] == 6) {?>
                    <li onclick="window.location='paginas-blocos-ficheiros.php?id=<?php echo $id; ?>&pagina=<?php echo $pagina; ?>&fixo=<?php echo $fixo; ?>'"><a href="javascript:void(null)"> <?php echo $RecursosCons->RecursosCons['tab_ficheiros']; ?> </a> </li>
                  <?php } 
                  if($row_rsP2['tipo'] == 7) {?>
                    <li onclick="window.location='paginas-blocos-timeline.php?id=<?php echo $id; ?>&pagina=<?php echo $pagina; ?>&fixo=<?php echo $fixo; ?>'"><a href="javascript:void(null)"> <?php echo $RecursosCons->RecursosCons['blocos_sel_timeline']; ?> </a> </li>
                  <?php } ?>
                  <?php /* <li <?php if($tab_sel==3) echo "class=\"active\""; ?> ><a href="#tab_fundo" data-toggle="tab" onClick="document.getElementById('tab_sel').value='3'"> <?php echo $RecursosCons->RecursosCons['tab_fundo']; ?> </a> </li> */ ?>    
                </ul>
                <div class="tab-content no-space">
                  <div class="tab-pane <?php if($tab_sel==1) echo "active"; ?>" id="tab_detalhes">
                    <div class="form-body">
                      <div class="alert alert-danger display-<?php if($erro_insert == 1) { echo "show"; } else { echo "hide"; } ?>">
                        <button class="close" data-close="alert"></button>
                        <?php echo $RecursosCons->RecursosCons['msg_required']; ?> 
                      </div> 
                      <div class="alert alert-success display-<?php if($erro_insert == 0 && $inserido == 1) { echo "show"; } else { echo "hide"; } ?>">
                        <button class="close" data-close="alert"></button>
                        <?php echo $RecursosCons->RecursosCons['alt']; ?> 
                      </div>  
                      <div class="form-group">
                        <label class="col-md-2 control-label" for="tipo"><?php echo $RecursosCons->RecursosCons['tipo_label']; ?>: <span class="required"> * </span></label>
                        <div class="col-md-8">
                          <select class="form-control select2me" id="tipo" name="tipo" disabled>
                            <option value="0"><?php echo $RecursosCons->RecursosCons['blocos_sel_tipo']; ?></option>
                            <option value="1" <?php if($row_rsP2['tipo'] == 1) echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['blocos_sel_textoimg']; ?></option>
                            <option value="2" <?php if($row_rsP2['tipo'] == 2) echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['blocos_sel_texto']; ?></option>
                            <option value="3" <?php if($row_rsP2['tipo'] == 3) echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['blocos_sel_2videos']; ?></option>
                            <option value="4" <?php if($row_rsP2['tipo'] == 4) echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['blocos_sel_google_maps']; ?></option>
                            <option value="5" <?php if($row_rsP2['tipo'] == 5) echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['blocos_sel_formulario']; ?></option>
                            <option value="6" <?php if($row_rsP2['tipo'] == 6) echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['blocos_sel_ficheiros']; ?></option>
                            <option value="7" <?php if($row_rsP2['tipo'] == 7) echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['blocos_sel_timeline']; ?></option>
                          </select>
                        </div>
                      </div>                
                      <div class="form-group">
                        <label class="col-md-2 control-label" for="nome"><?php echo $RecursosCons->RecursosCons['nome_label']; ?>: <span class="required"> * </span></label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="nome" id="nome" value="<?php echo $row_rsP2['nome']; ?>">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label id="titulo" class="col-md-2 control-label" for="titulo"><?php echo $RecursosCons->RecursosCons['titulo_label']; ?>:</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo $row_rsP2['titulo']; ?>">
                        </div>
                      </div> 
                      <div class="outro_tipos">
                        <?php if($row_rsP2['tipo'] == 2 || $row_rsP2['tipo'] == 3) { ?>
                          <div class="form-group">
                            <label class="col-md-2 control-label" for="colunas"><?php echo $RecursosCons->RecursosCons['colunas_label']; ?>: </label>
                            <div class="col-md-8 md-radio-inline">
                              <?php if($row_rsP2['tipo'] == 2){ ?>
                                <div class="md-radio">
                                  <input id="radio1" name="colunas" value="1" <?php if($row_rsP2['colunas']==1) echo "checked"; ?> class="md-radiobtn radio_colunas" type="radio">
                                  <label for="radio1">
                                  <span></span>
                                  <span class="check"></span>
                                  <span class="box"></span>
                                  1 </label>
                                </div>
                              <?php } ?>
                              <div class="md-radio">
                                <input id="radio2" name="colunas" value="2" <?php if($row_rsP2['colunas']==2 && ($row_rsP2['tipo'] == 3 || $row_rsP2['tipo'] == 2)) echo "checked"; ?>  class="md-radiobtn radio_colunas" type="radio">
                                <label for="radio2">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                2 </label>
                              </div>
                              <div class="md-radio">
                                <input id="radio3" name="colunas" value="3" <?php if($row_rsP2['colunas']==3 && ($row_rsP2['tipo'] == 3 || $row_rsP2['tipo'] == 2)) echo "checked"; ?>  class="md-radiobtn radio_colunas" type="radio">
                                <label for="radio3">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                3 </label>
                              </div>
                            </div>
                          </div>
                        <?php } ?>
                        <?php if($row_rsP2['tipo'] == 1) { ?>
                          <div class="form-group">
                            <label class="col-md-2 control-label" for="orientacao"><?php echo $RecursosCons->RecursosCons['orientacao_label']; ?>: </label>
                            <div class="col-md-8 md-radio-inline">
                              <div class="md-radio">
                                <input id="radio1" name="orientacao" value="0" <?php if($row_rsP2['orientacao']==0) echo "checked"; ?> class="md-radiobtn radio_orientacao" type="radio">
                                <label for="radio1">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['texto_esquerda']; ?> </label>
                              </div>
                              <div class="md-radio">
                                <input id="radio2" name="orientacao" value="1" <?php if($row_rsP2['orientacao']==1) echo "checked"; ?>  class="md-radiobtn radio_orientacao" type="radio">
                                <label for="radio2">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['texto_direita']; ?>  </label>
                              </div>
                              <div class="md-radio">
                                <input id="radio3" name="orientacao" value="2" <?php if($row_rsP2['orientacao']==2) echo "checked"; ?>  class="md-radiobtn radio_orientacao" type="radio">
                                <label for="radio3">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['texto_por_baixo']; ?> </label>
                              </div>
                              <div class="md-radio">
                                <input id="radio4" name="orientacao" value="3" <?php if($row_rsP2['orientacao']==3) echo "checked"; ?>  class="md-radiobtn radio_orientacao" type="radio">
                                <label for="radio4">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['texto_por_cima']; ?> </label>
                              </div>
                            </div>
                          </div>
                          <div id="div_contorna" class="form-group">
                            <label class="col-md-2 control-label" for="contorna"><?php echo $RecursosCons->RecursosCons['texto_contornar']; ?> </label>
                            <div class="col-md-8 md-radio-inline">
                              <div class="md-radio">
                                <input id="radio5" name="contorna" value="0" <?php if($row_rsP2['texto_contorna']==0) echo "checked"; ?>  class="md-radiobtn radio_contorna" type="radio">
                                <label for="radio5">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['pesq_nao']; ?>  </label>
                              </div>
                              <div class="md-radio">
                                <input id="radio6" name="contorna" value="1" <?php if($row_rsP2['texto_contorna']==1) echo "checked"; ?>  class="md-radiobtn radio_contorna" type="radio">
                                <label for="radio6">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['pesq_sim']; ?>  </label>
                              </div>
                            </div>
                          </div>
                        <?php } ?>
                        <?php if($row_rsP2['tipo'] == 1 || $row_rsP2['tipo'] == 2){ ?>
                          <div class="form-group" id="div_largura_texto">
                            <label class="col-md-2 control-label" for="largura_texto"><?php echo $RecursosCons->RecursosCons['largura_texto_label']; ?>: </label>
                            <div class="col-md-3 md-radio-inline">
                              <div class="md-radio">
                                <input id="largura_texto1" name="largura_texto" value="1" <?php if($row_rsP2['largura_texto']==1) echo "checked"; ?> class="md-radiobtn radio_largura_texto" type="radio">
                                <label for="largura_texto1">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['largura_max']; ?> </label>
                              </div>
                              <div class="md-radio">
                                <input id="largura_texto2" name="largura_texto" value="2" <?php if($row_rsP2['largura_texto']==2) echo "checked"; ?>  class="md-radiobtn radio_largura_texto" type="radio">
                                <label for="largura_texto2">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['largura_especifica']; ?>  </label>
                              </div>
                            </div>
                            <label class="col-md-2 control-label valor-largura-texto" for="valor_largura_texto"><?php echo $RecursosCons->RecursosCons['valor_largura_label']; ?>: </label>
                            <div class="col-md-2 valor-largura-texto">
                              <div class="input-group">
                                <input type="text" class="form-control" name="valor_largura_texto" id="valor_largura_texto" value="<?php echo $row_rsP2['valor_largura_texto']; ?>">
                                <span id="span_desconto" class="input-group-addon">px</span>
                              </div> 
                            </div>
                          </div>
                        <?php } ?>
                        <?php if($row_rsP2['tipo'] == 5) { ?>
                          <div class="form-group">
                            <label class="col-md-2 control-label" for="orientacao"><?php echo $RecursosCons->RecursosCons['orientacao_label']; ?></label>
                            <div class="col-md-8 md-radio-inline">
                              <div class="md-radio">
                                <input id="radio1" name="orientacao" value="0" <?php if($row_rsP2['orientacao']==0) echo "checked"; ?> class="md-radiobtn radio_orientacao" type="radio">
                                <label for="radio1">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['form_esquerda']; ?></label>
                              </div>
                              <div class="md-radio">
                                <input id="radio2" name="orientacao" value="1" <?php if($row_rsP2['orientacao']==1) echo "checked"; ?>  class="md-radiobtn radio_orientacao" type="radio">
                                <label for="radio2">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['form_direita']; ?> </label>
                              </div>
                              <div class="md-radio">
                                <input id="radio3" name="orientacao" value="2" <?php if($row_rsP2['orientacao']==2) echo "checked"; ?>  class="md-radiobtn radio_orientacao" type="radio">
                                <label for="radio3">
                                <span></span>
                                <span class="check"></span>
                                <span class="box"></span>
                                <?php echo $RecursosCons->RecursosCons['form_por_baixo']; ?></label>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label id="texto1_label" class="col-md-2 control-label" for="texto">Texto: </label>
                            <div class="col-md-8">
                               <textarea class="form-control" name="texto" id="texto" style="resize:none;height:250px"><?php echo $row_rsP2['texto']; ?></textarea>
                            </div>
                          </div> 
                        <?php } ?>
                        <?php if($row_rsP2['tipo'] == 2 || $row_rsP2['tipo'] == 1 || $row_rsP2['tipo'] == 3 || $row_rsP2['tipo'] == 4 || $row_rsP2['tipo'] == 6) { ?>
                          <?php if($row_rsP2['colunas'] == 2 || $row_rsP2['colunas'] == 3) { ?>
                            <style type="text/css">
                              #texto1_div {
                                display: block;
                              }
                            </style>
                          <?php } else { ?>
                            <style type="text/css">
                              #texto1_div {
                                display: none;
                              }
                            </style>
                          <?php } ?>
                          <div id="texto1_div" class="form-group">
                            <label class="col-md-2 control-label" for="titulo1"><?php echo $RecursosCons->RecursosCons['titulo1_label']; ?>: </label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" name="titulo1" id="titulo1" value="<?php echo $row_rsP2['titulo1']; ?>"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label id="texto1_label" class="col-md-2 control-label" for="texto"><?php echo $RecursosCons->RecursosCons['texto_label']; ?>: </label>
                            <div class="col-md-8">
                               <textarea class="form-control" name="texto" id="texto" style="resize:none;height:250px"><?php echo $row_rsP2['texto']; ?></textarea>
                            </div>
                          </div>
                          <div class="form-group cta1">
                            <label class="col-md-2 control-label" for="link1"><?php echo $RecursosCons->RecursosCons['link_label']; ?>: </label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" name="link1" id="link1" value="<?php echo $row_rsP2['link1']; ?>"/>
                            </div>
                          </div>
                          <div class="form-group cta1">
                            <label class="col-md-2 control-label" for="target1"><?php echo $RecursosCons->RecursosCons['target_link']; ?>: </label>
                            <div class="col-md-3">
                              <select class="form-control" name="target1" id="target1">
                                <option value=""><?php echo $RecursosCons->RecursosCons['opt_selecionar']; ?></option>
                                <option value="_parent" <?php if($row_rsP2['target1'] == '_parent') echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['opt_mesma-janela']; ?></option>
                                <option value="_blank" <?php if($row_rsP2['target1'] == '_blank') echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['opt_nova_janela']; ?></option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group cta1">
                            <label class="col-md-2 control-label" for="texto_botao1"><?php echo $RecursosCons->RecursosCons['texto_botao_label']; ?>: </label>
                            <div class="col-md-3">
                              <input type="text" class="form-control" name="texto_botao1" id="texto_botao1" value="<?php echo $row_rsP2['texto_botao1']; ?>"/>
                            </div>
                          </div>
                          <?php if($row_rsP2['colunas'] == 2 || $row_rsP2['colunas'] == 3) { ?>
                            <style type="text/css">
                              #texto2_div {
                                display: block;
                              }
                            </style>
                          <?php } ?>
                          <div id="texto2_div">
                            <hr>
                            <div class="form-group">
                              <label class="col-md-2 control-label" for="titulo2"><?php echo $RecursosCons->RecursosCons['titulo2_label']; ?>: </label>
                              <div class="col-md-8">
                                <input type="text" class="form-control" name="titulo2" id="titulo2" value="<?php echo $row_rsP2['titulo2']; ?>"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <label id="texto2_label" class="col-md-2 control-label" for="texto2"><?php echo $RecursosCons->RecursosCons['coluna2_label']; ?>: </label>
                              <div class="col-md-8">
                                 <textarea class="form-control" name="texto2" id="texto2" style="resize:none;height:250px"><?php echo $row_rsP2['texto2']; ?></textarea>
                              </div>
                            </div>
                          </div>
                          <!-- CAT 2 -->
                          <div class="form-group cta2">
                            <label class="col-md-2 control-label" for="link2"><?php echo $RecursosCons->RecursosCons['link_label']; ?> 2: </label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" name="link2" id="link2" value="<?php echo $row_rsP2['link2']; ?>"/>
                            </div>
                          </div>
                          <div class="form-group cta2">
                            <label class="col-md-2 control-label" for="target2"><?php echo $RecursosCons->RecursosCons['target_link']; ?> 2: </label>
                            <div class="col-md-3">
                              <select class="form-control" name="target2" id="target2">
                                <option value=""><?php echo $RecursosCons->RecursosCons['opt_selecionar']; ?></option>
                                <option value="_parent" <?php if($row_rsP2['target2'] == '_parent') echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['opt_mesma-janela']; ?></option>
                                <option value="_blank" <?php if($row_rsP2['target2'] == '_blank') echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['opt_nova_janela']; ?></option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group cta2">
                            <label class="col-md-2 control-label" for="texto_botao2"><?php echo $RecursosCons->RecursosCons['texto_botao_label']; ?> 2: </label>
                            <div class="col-md-3">
                              <input type="text" class="form-control" name="texto_botao2" id="texto_botao2" value="<?php echo $row_rsP2['texto_botao2']; ?>"/>
                            </div>
                          </div>
                          <?php if($row_rsP2['colunas'] == 3) { ?>
                            <style type="text/css">
                              #texto3_div {
                                display: block;
                              }
                            </style>
                          <?php } ?>
                          <div id="texto3_div">
                            <hr>
                            <div class="form-group">
                              <label class="col-md-2 control-label" for="titulo3"><?php echo $RecursosCons->RecursosCons['titulo3_label']; ?>: </label>
                              <div class="col-md-8">
                                <input type="text" class="form-control" name="titulo3" id="titulo3" value="<?php echo $row_rsP2['titulo3']; ?>"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <label id="texto3_label" class="col-md-2 control-label" for="texto3"><?php echo $RecursosCons->RecursosCons['coluna3_label']; ?>: </label>
                              <div class="col-md-8">
                                 <textarea class="form-control" name="texto3" id="texto3" style="resize:none;height:250px"><?php echo $row_rsP2['texto3']; ?></textarea>
                              </div>
                            </div>
                          </div>
                          <!-- CTA 3 -->
                          <div class="form-group cta3">
                            <label class="col-md-2 control-label" for="link3"><?php echo $RecursosCons->RecursosCons['link_label']; ?> 3: </label>
                            <div class="col-md-8">
                              <input type="text" class="form-control" name="link3" id="link3" value="<?php echo $row_rsP2['link3']; ?>"/>
                            </div>
                          </div>
                          <div class="form-group cta3">
                            <label class="col-md-2 control-label" for="target3"><?php echo $RecursosCons->RecursosCons['target_link']; ?> 3: </label>
                            <div class="col-md-3">
                              <select class="form-control" name="target3" id="target3">
                                <option value=""><?php echo $RecursosCons->RecursosCons['opt_selecionar']; ?></option>
                                <option value="_parent" <?php if($row_rsP2['target3'] == '_parent') echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['opt_mesma-janela']; ?></option>
                                <option value="_blank" <?php if($row_rsP2['target3'] == '_blank') echo "selected"; ?>><?php echo $RecursosCons->RecursosCons['opt_nova_janela']; ?></option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group cta3">
                            <label class="col-md-2 control-label" for="texto_botao3"><?php echo $RecursosCons->RecursosCons['texto_botao_label']; ?> 3: </label>
                            <div class="col-md-3">
                              <input type="text" class="form-control" name="texto_botao3" id="texto_botao3" value="<?php echo $row_rsP2['texto_botao3']; ?>"/>
                            </div>
                          </div>
                        <?php } ?>
                        <?php if($row_rsP2['tipo'] == 4) { ?>
                          <div class="form-group">
                            <label class="col-md-2 control-label" for="mapa"><?php echo $RecursosCons->RecursosCons['mapa_label']; ?>: </label>
                            <div class="col-md-8">
                              <textarea rows="4" class="form-control" name="mapa" id="mapa"><?php echo $row_rsP2['mapa']; ?></textarea>
                              <p class="help-block">< iframe src="https://www.google.com/maps/d/embed?mid=XXXXXXXX" frameborder="0" allowfullscreen width="100%" height="100%"></ iframe></p>
                            </div>
                          </div>
                        <?php }�?>
                        <?php /*if($row_rsP2['tipo'] == 2 || $row_rsP2['tipo'] == 3) { ?>
                          <hr>
                          <?php if($row_rsP2['tipo'] == 2) { ?>
                            <div class="form-group">
                              <label class="col-md-2 control-label" for="fullscreen"><?php echo $RecursosCons->RecursosCons['galeria_fullscreen_label']; ?> </label>
                              <div class="col-md-8" style="padding-top: 8px">
                                <div class="checkbox-list">
                                  <label>
                                  <input type="checkbox" name="fullscreen" id="fullscreen" <?php if($row_rsP2['fullscreen'] == 1) echo "checked"; ?>></label>
                                </div>
                                <p class="help-block"><?php echo $RecursosCons->RecursosCons['info_galeria_msg']; ?></p>
                              </div>
                            </div>
                          <?php } ?>
                          <div class="form-group">
                            <label class="col-md-2 control-label" for="tipo_galeria"><?php echo $RecursosCons->RecursosCons['modo_galeria']; ?> </label>
                            <div class="col-md-8" style="padding-top: 8px">
                              <div class="checkbox-list">
                                <label>
                                <input type="checkbox" name="tipo_galeria" id="tipo_galeria" <?php if($row_rsP2['tipo_galeria'] == 1) echo "checked"; ?>></label>
                              </div>
                              <p class="help-block"><?php echo $RecursosCons->RecursosCons['info_galeria_msg_2']; ?></p>
                            </div>
                          </div>
                          <div id="div_slideshow" class="form-group">
                            <label class="col-md-2 control-label" for="tipo_imagens"><?php echo $RecursosCons->RecursosCons['slideshow_label']; ?> </label>
                            <div class="col-md-8" style="padding-top: 8px">
                              <div class="checkbox-list">
                                <label>
                                <input type="checkbox" name="tipo_imagens" id="tipo_imagens" <?php if($row_rsP2['tipo_imagens'] == 1) echo "checked"; ?>></label>
                              </div>
                              <p class="help-block"><?php echo $RecursosCons->RecursosCons['info_slideshow']; ?></p>
                            </div>
                          </div>
                          <div id="div_esp_imagens" class="form-group">
                            <label class="col-md-2 control-label" for="esp_imagens"><?php echo $RecursosCons->RecursosCons['espacamento_imagens']; ?>: </label>
                            <div class="col-md-2">
                              <div class="input-group">
                                <input type="text" class="form-control" name="esp_imagens" id="esp_imagens" value="<?php echo $row_rsP2['esp_imagens']; ?>" onkeyup="onlyNumber(this)" onblur="onlyNumber(this)">
                                <span class="input-group-addon">px</span>
                              </div>
                            </div>
                          </div>
                        <?php } */ ?>
                      </div>         
                    </div>
                  </div>
                  <div class="tab-pane <?php if($tab_sel==3) echo "active"; ?>" id="tab_fundo">
                    <div class="form-body">
                      <?php if(isset($_GET['alt']) && $_GET['alt'] == 1 && $tab_sel == 3) { ?>
                        <div class="alert alert-success display-show">
                          <button class="close" data-close="alert"></button>
                          <span>  <?php echo $RecursosCons->RecursosCons['alt']; ?> </span>
                        </div>
                      <?php } ?>
                      <?php if(isset($_GET['env']) && $_GET['env'] == 1 && $tab_sel == 3) { ?>  
                        <div class="alert alert-success display-show">
                        <button class="close" data-close="alert"></button>
                         <?php echo $RecursosCons->RecursosCons['env_config']; ?> </div>
                      <?php }�?>
                      <div class="form-group div_topo" style="margin-top: 40px;">
                          <label class="col-md-2 control-label" for="tem_fundo" style="padding-top:0;"><?php echo $RecursosCons->RecursosCons['com_fundo']; ?> </label>
                          <div class="col-md-10">
                            <input type="checkbox" class="form-control" name="tem_fundo" id="tem_fundo" value="1" <?php if($row_rsP2['tem_fundo'] == 1) echo "checked";?>>
                            <p class="help-block"><?php echo $RecursosCons->RecursosCons['info_cor_fundo']; ?></p>
                          </div>
                        </div>
                      <div class="form-group div_topo" id="div_tipo">
                        <label class="col-md-2 control-label" for="tipo_fundo"><?php echo $RecursosCons->RecursosCons['tipo_fundo_label']; ?>: </label>
                        <div class="col-md-3" style="padding-top: 6px;">
                          <div class="radio-list">
                            <label class="radio" style="margin-bottom: 10px;">
                            <input type="radio" name="tipo_fundo" id="tipo_fundo1" value="1" <?php if($row_rsP2['tipo_fundo'] == 1) echo "checked"; ?>> <?php echo $RecursosCons->RecursosCons['cor_label']; ?> </label>
                            <label class="radio">
                            <input type="radio" name="tipo_fundo" id="tipo_fundo2" value="2" <?php if($row_rsP2['tipo_fundo'] == 2) echo "checked"; ?>> <?php echo $RecursosCons->RecursosCons['imagem']; ?> </label>
                          </div>
                        </div>
                        <label class="col-md-2 control-label div_cor_fundo" for="cor_fundo"><?php echo $RecursosCons->RecursosCons['cor_fundo_label']; ?>: </label>
                        <div class="col-md-2 div_cor_fundo">
                          <input type="text" class="form-control color" name="cor_fundo" id="cor_fundo" value="<?php echo $row_rsP2['cor_fundo']; ?>">
                        </div>
                        <div class="col-md-3 div_cor_fundo">
                          <strong style="display: block; margin-bottom: 10px;"><?php echo $RecursosCons->RecursosCons['cores_website_label']; ?></strong>
                          <span style="display: inline-block; width: 50px; margin-bottom: 10px;"><?php echo $RecursosCons->RecursosCons['primeira_cor']; ?>:</span> <span style="display: inline-block; width: 65px; height: 25px; background-color: #EEEEEE; color: #000000; padding: 3px 5px;">#EEEEEE</span><br>
                          <span style="display: inline-block; width: 50px; margin-bottom: 10px;"><?php echo $RecursosCons->RecursosCons['segunda_cor']; ?>:</span> <span style="display: inline-block; width: 65px; height: 25px; background-color: #A0D6FF; color: #000000; padding: 3px 5px;">#A0D6FF</span><br>
                          <span style="display: inline-block; width: 50px; margin-bottom: 10px;"><?php echo $RecursosCons->RecursosCons['terceira_cor']; ?>:</span> <span style="display: inline-block; width: 65px; height: 25px; background-color: #0299D8; color: #FFFFFF; padding: 3px 5px;">#0299D8</span><br>
                          <span style="display: inline-block; width: 50px; margin-bottom: 10px;"><?php echo $RecursosCons->RecursosCons['quarta_cor']; ?>:</span> <span style="display: inline-block; width: 65px; height: 25px; background-color: #F36A2F; color: #FFFFFF; padding: 3px 5px;">#F36A2F</span>
                        </div>
                      </div>
                      <div class="form-group div_imagem">
                        <label class="col-md-2 control-label" for="mascara_fundo" style="padding-top:0;"> <?php echo $RecursosCons->RecursosCons['tem_mascara_label']; ?></label>
                        <div class="col-md-4">
                          <input type="checkbox" class="form-control" name="mascara_fundo" id="mascara_fundo" value="1" <?php if($row_rsP2['mascara_fundo'] == 1) echo "checked";?>>
                          <p class="help-block"><?php echo $RecursosCons->RecursosCons['sel_mascara_msg']; ?></p>
                        </div>
                      </div>
                      <div class="form-group div_topo div_imagem">
                        <label class="col-md-2 control-label" style="text-align:right"><?php echo $RecursosCons->RecursosCons['imagem']; ?><br>
                          <strong><?php echo $tamanho_imagens1['0']." * ".$tamanho_imagens1['1']." px"; ?>:</strong> </label>
                        <div class="col-md-4">
                          <div class="fileinput fileinput-<?php if($row_rsP2['imagem_fundo']!="" && file_exists("../../../imgs/paginas/".$row_rsP2['imagem_fundo'])) { ?>exists<?php } else { ?>new<?php } ?>" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>imgs/sem_imagem.png" alt=""/> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                              <?php if($row_rsP2['imagem_fundo']!="" && file_exists("../../../imgs/paginas/".$row_rsP2['imagem_fundo'])) { ?>
                              <a href="../../../imgs/paginas/<?php echo $row_rsP2['imagem_fundo']; ?>" data-fancybox ><img src="../../../imgs/paginas/<?php echo $row_rsP2['imagem_fundo']; ?>"></a>
                              <?php } ?>
                            </div>
                            <div> <span class="btn default btn-file"> <span class="fileinput-new"> <?php echo $RecursosCons->RecursosCons['selec_imagem']; ?> </span> <span class="fileinput-exists"> <?php echo $RecursosCons->RecursosCons['btn_altera_img']; ?> </span>
                              <input id="upload_campo" type="file" name="img">
                              </span> <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput" onClick="document.getElementById('img_remover1').value='1'"> <?php echo $RecursosCons->RecursosCons['btn_remove_img']; ?> </a> </div>
                          </div>
                          <div class="clearfix margin-top-10"> <span class="label label-danger"><?php echo $RecursosCons->RecursosCons['formatos_sup_txt']; ?></span> </div>
                          <script type="text/javascript">
                          function alterar_imagem(){
                              document.getElementById('file_delete_1').value='';
                          }
                          function remover_imagem(){
                              document.getElementById('file_delete_1').value='';
                              document.getElementById('img_cont_1_vazia').style.display='block';                  
                              document.getElementById('img_cont_1').style.display='none';
                          }
                          </script><br><br>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" name="MM_insert" value="paginas_blocos_form" />
          </form>
        </div>
      </div>
      <!-- END PAGE CONTENT--> 
    </div>
  </div>
  <!-- END CONTENT -->
  <?php include_once(ROOTPATH_ADMIN.'inc_quick_sidebar.php'); ?>
</div>
<!-- END CONTAINER -->
<?php include_once(ROOTPATH_ADMIN.'inc_footer_1.php'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS --> 
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script> 
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script> 
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/select2/select2.min.js"></script> 
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script> 
<script src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script> 
<script src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/fancybox/jquery.fancybox.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<?php include_once(ROOTPATH_ADMIN.'inc_footer_2.php'); ?>
<script src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo ROOTPATH_HTTP_CONSOLA; ?>js/jscolor/jscolor.js"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS --> 
<script src="form-validation.js"></script> 
<!-- END PAGE LEVEL SCRIPTS --> 
<script>
jQuery(document).ready(function() {    
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  QuickSidebar.init(); // init quick sidebar
  Demo.init(); // init demo features
  FormValidation.init();

  if('<?php echo $row_rsP2["tem_fundo"]; ?>' == '0') {
    $('#div_tipo').css('display', 'none');
    $('.div_imagem').css('display', 'none');
  }
  else {
    $('#div_tipo').css('display', 'block');
  }

  if('<?php echo $row_rsP2["largura_texto"]; ?>' == '1') {
    $('.valor-largura-texto').css('display', 'none');
  }
  else if('<?php echo $row_rsP2["largura_texto"]; ?>' == '2') {
    $('.valor-largura-texto').css('display', 'block');
  }

  if('<?php echo $row_rsP2["colunas"]; ?>' == '1') {
    $('#div_largura_texto').css('display', 'block');
  }
  else if('<?php echo $row_rsP2["colunas"]; ?>' == '2') {
    $('#div_largura_texto').css('display', 'none');
  }
  else if('<?php echo $row_rsP2["colunas"]; ?>' == '3') {
    $('#div_largura_texto').css('display', 'none');
  }   

  $('#tem_fundo').on('change', function() {
    if($(this).is(':checked')) {
      $('#div_tipo').css('display', 'block');
    }
    else {
      $('#div_tipo').css('display', 'none');
      $('.div_imagem').css('display', 'none');
    }
  });

  if('<?php echo $row_rsP2["tem_fundo"]; ?>' == '1' && '<?php echo $row_rsP2["tipo_fundo"]; ?>' == '2') {
    $('.div_imagem').css('display', 'block');
    $('.div_cor_fundo').css('display', 'none');
  }
  else {
    $('.div_imagem').css('display', 'none');
    $('.div_cor_fundo').css('display', 'inline-block');
  }

  $('input[type=radio][name=tipo_fundo]').change(function() {
    if(this.value == '1') {
      $('.div_imagem').css('display', 'none');
      $('.div_cor_fundo').css('display', 'inline-block');
    }
    else if(this.value == '2') {
      $('.div_imagem').css('display', 'block');
      $('.div_cor_fundo').css('display', 'none');
    }
  });

  if('<?php echo $row_rsP2["orientacao"]; ?>' == '2' || '<?php echo $row_rsP2["orientacao"]; ?>' == '3'){
    $('#div_contorna').css('display', 'none');
    $('#div_largura_texto').css('display', 'block');
  }
  else if('<?php echo $row_rsP2["orientacao"]; ?>' == '0' || '<?php echo $row_rsP2["orientacao"]; ?>' == '1'){
    $('#div_contorna').css('display', 'block');
    $('#div_largura_texto').css('display', 'none');
  }

  $('.radio_orientacao').on('change', function() {
    var id = $(this).attr('id');

    if(id == 'radio3' || id == 'radio4'){
      $('#div_contorna').css('display', 'none');
      $('#div_largura_texto').css('display', 'block');
    }
    else{
      $('#div_contorna').css('display', 'block');
      $('#div_largura_texto').css('display', 'none');
    }
  });

  $('.radio_largura_texto').on('change', function() {
    var id2 = $(this).attr('id');

    if(id2 == 'largura_texto1'){
      $('.valor-largura-texto').css('display', 'none');
    }
    else if(id2 == 'largura_texto2'){
      $('.valor-largura-texto').css('display', 'block');
    }
  });

  $('.radio_colunas').on('change', function() {
    var id = $(this).attr('id');

    if(id == 'radio3') {
      $('#texto1_div').css('display', 'block');
      $('#texto2_div').css('display', 'block');
      $('#texto3_div').css('display', 'block');
      $('#texto1_label').html('Coluna 1 <span class="required">*</span>');
      $('.cta2').css('display', 'block');
      $('.cta3').css('display', 'block');
      $('#div_largura_texto').css('display', 'none');
    }
    else if(id == 'radio2'){
      $('#texto1_div').css('display', 'block');
      $('#texto2_div').css('display', 'block');
      $('#texto3_div').css('display', 'none');
      $('#texto1_label').html('Coluna 1 <span class="required">*</span>');
      $('.cta2').css('display', 'block');
      $('.cta3').css('display', 'none');
      $('#div_largura_texto').css('display', 'none');
    }
    else {
      $('#texto1_div').css('display', 'none');
      $('#texto2_div').css('display', 'none');
      $('#texto3_div').css('display', 'none');
      $('#texto1_label').html('Texto <span class="required">*</span>');
      $('#div_largura_texto').css('display', 'block');
      $('.cta2').css('display', 'none');
      $('.cta3').css('display', 'none');
    }
  });

  if($('#texto2_div').is(":visible")) {
    $('#texto1_label').html('Coluna 1 <span class="required">*</span>');
  }

  if($('#tipo').val() == 1 || $('#tipo').val() == 2) {
    $('#titulo').text('T�tulo');
    $('.outros_tipos').css('display', 'none');
    $('.cta1').css('display', 'block');
    $('.cta2').css('display', 'none');
    $('.cta3').css('display', 'none');

    if('<?php echo $row_rsP2["colunas"]; ?>' == '2'){
      $('.cta2').css('display', 'block');
    }
    else if('<?php echo $row_rsP2["colunas"]; ?>' == '3'){
      $('.cta2').css('display', 'block');
      $('.cta3').css('display', 'block');
    }    
  }
  else if($('#tipo').val() == 3){
    $('#texto1_div').css('display', 'block');
    $('#texto2_div').css('display', 'block');
    $('#texto1_label').html('Coluna 1 <span class="required">*</span>');
    $('.cta1').css('display', 'block');
    $('.cta2').css('display', 'block');
    if('<?php echo $row_rsP2["colunas"]; ?>' == '3'){
      $('.cta3').css('display', 'block');
    }
    else {
      $('.cta3').css('display', 'none');
    }
  }
  else if($('#tipo').val() == 4){
    $('.outros_tipos').css('display', 'none');
    $('.cta1').css('display', 'none');
    $('.cta2').css('display', 'none');
    $('.cta3').css('display', 'none');
  }
  else if($('#tipo').val() == 6){
    $('.outros_tipos').css('display', 'none');
    $('.cta1').css('display', 'none');
    $('.cta2').css('display', 'none');
    $('.cta3').css('display', 'none');
  }
  else {
    $('#titulo').text('T�tulo');
    $('.tipo5').css('display', 'none');
    $('.outros_tipos').css('display', 'block');
    $('.tipo_botao').css('display', 'none');
  }

  if($('#tipo_galeria').is(':checked')) {
    $('#div_video').css('display', 'none');
    $('#div_slideshow').css('display', 'block');
    
    if($('#tipo_imagens').is(':checked')) {
      $('#div_esp_imagens').css('display', 'none');
    }
    else {
      $('#div_esp_imagens').css('display', 'block');
    }
  }
  else {
    $('#div_slideshow').css('display', 'none');
    $('#div_esp_imagens').css('display', 'none');
  }

  $('#tipo_galeria').on('change', function() {
    if($('#tipo_galeria').is(':checked')) {
      $('#div_video').css('display', 'none');
      $('#div_slideshow').css('display', 'block');
      
      if($('#tipo_imagens').is(':checked')) {
        $('#div_esp_imagens').css('display', 'none');
      }
      else {
        $('#div_esp_imagens').css('display', 'block');
      }
    }
    else {
      $('#div_slideshow').css('display', 'none');
      $('#div_esp_imagens').css('display', 'none');
    }
  });

  $('#tipo_imagens').on('change', function() {
    if($('#tipo_imagens').is(':checked')) {
      $('#div_esp_imagens').css('display', 'none');
    }
    else {
      $('#div_esp_imagens').css('display', 'block');
    }
  });
});
</script>
<script type="text/javascript">
CKEDITOR.replace('texto',
{
  filebrowserBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html',
  filebrowserImageBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html?Type=Images',
  filebrowserFlashBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html?Type=Flash',
  filebrowserUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
  filebrowserImageUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
  filebrowserFlashUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
  height: '250px',
  toolbar : "Basic"
});
CKEDITOR.replace('texto2',
{
  filebrowserBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html',
  filebrowserImageBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html?Type=Images',
  filebrowserFlashBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html?Type=Flash',
  filebrowserUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
  filebrowserImageUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
  filebrowserFlashUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
  height: '250px',
  toolbar : "Basic"
});
CKEDITOR.replace('texto3',
{
  filebrowserBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html',
  filebrowserImageBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html?Type=Images',
  filebrowserFlashBrowseUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/ckfinder.html?Type=Flash',
  filebrowserUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
  filebrowserImageUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
  filebrowserFlashUploadUrl : '<?php echo ROOTPATH_HTTP_CONSOLA; ?>assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
  height: '250px',
  toolbar : "Basic"
});
</script> 
</body>
<!-- END BODY -->
</html>