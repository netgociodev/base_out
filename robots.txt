# www.robotstxt.org/
# http://code.google.com/web/controlcrawlindex/

User-agent: * 
Disallow: /*
#Sitemap: http://www.NOMESITE.com/sitemap/sitemap.xml

Disallow: /Connections/
Disallow: /consola/*
Disallow: /imgs/recrutamento/
Disallow: /imgs/downloads/
Disallow: /imgs/tickets/
Disallow: /linguas/
Disallow: /sendMail/