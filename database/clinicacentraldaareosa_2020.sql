-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 27-Abr-2020 às 11:52
-- Versão do servidor: 5.5.54-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `clinicacentraldaareosa_2020`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acesso`
--

CREATE TABLE IF NOT EXISTS `acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `super_administrador` tinyint(4) DEFAULT '0',
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `password_salt` varchar(10) DEFAULT NULL,
  `cod_recupera` varchar(255) DEFAULT NULL,
  `nivel` tinyint(4) DEFAULT '1',
  `nome` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `funcao` varchar(40) DEFAULT NULL,
  `observacoes` text,
  `imagem1` varchar(250) DEFAULT '041415_1_2194_joaninha.jpg',
  `last_activity` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `notif_from` datetime DEFAULT NULL,
  `lingua` varchar(20) DEFAULT 'pt',
  `n_enc` int(11) DEFAULT NULL,
  `n_tick` int(11) DEFAULT NULL,
  `n_cli` int(11) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`username`,`activo`,`email`,`super_administrador`,`last_activity`,`last_login`,`notif_from`,`n_enc`,`n_tick`,`n_cli`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `acesso`
--

INSERT INTO `acesso` (`id`, `super_administrador`, `username`, `password`, `password_salt`, `cod_recupera`, `nivel`, `nome`, `email`, `telefone`, `funcao`, `observacoes`, `imagem1`, `last_activity`, `last_login`, `notif_from`, `lingua`, `n_enc`, `n_tick`, `n_cli`, `activo`) VALUES
(1, 1, 'netg', 'a31328e1b6df31425f6e97467e5993e5e7d3cfcce7322ec7cf2d1267a6d04954', '7f5', NULL, 1, 'Administrador Master', 'marcio.santos@netgocio.pt', '1332', '343', '563', '041415_1_2194_joaninha.jpg', '2015-10-12 15:19:06', '2015-10-12 15:18:53', '2015-10-12 15:18:53', 'pt', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `analytics`
--

CREATE TABLE IF NOT EXISTS `analytics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT '0',
  `ecommerce` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `analytics`
--

INSERT INTO `analytics` (`id`, `email`, `password`, `activo`, `ecommerce`) VALUES
(1, 'mail@gmail.com', 'password', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners_h_pt`
--

CREATE TABLE IF NOT EXISTS `banners_h_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` tinyint(4) DEFAULT '1' COMMENT '1 - Imagem; 2 - Vídeo',
  `nome` varchar(250) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `subtitulo` varchar(250) DEFAULT NULL,
  `video` text,
  `link_class` varchar(255) DEFAULT NULL COMMENT '"vazio", invert, invert2',
  `link` varchar(250) DEFAULT NULL,
  `target` varchar(250) DEFAULT '0',
  `texto_link` varchar(250) DEFAULT NULL,
  `cor1` varchar(50) DEFAULT NULL,
  `align_h1` varchar(255) DEFAULT 'center',
  `align_v1` varchar(255) DEFAULT 'center',
  `mascara1` tinyint(4) DEFAULT '0' COMMENT '0 - Não aplicar máscara; 1 - Aplicar',
  `cor2` varchar(50) DEFAULT NULL,
  `align_h2` varchar(255) DEFAULT 'center',
  `align_v2` varchar(255) DEFAULT 'center',
  `mascara2` tinyint(4) DEFAULT '0' COMMENT '0 - Não aplicar máscara; 1 - Aplicar',
  `imagem1` text,
  `imagem2` text,
  `imagem3` text,
  `datai` date DEFAULT NULL,
  `dataf` date DEFAULT NULL,
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '0',
  `text_alignv` varchar(50) DEFAULT NULL COMMENT 'top, middle, bottom',
  `text_alignh` varchar(50) DEFAULT NULL COMMENT 'left, center, right',
  PRIMARY KEY (`id`),
  KEY `indice` (`tipo`,`datai`,`dataf`,`ordem`,`visivel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dominio` varchar(50) DEFAULT NULL,
  `url_metatags` varchar(50) NOT NULL,
  `manutencao` int(11) NOT NULL DEFAULT '0',
  `ips` text,
  `imagem_popup` varchar(255) DEFAULT NULL,
  `link_popup` varchar(255) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT '0' COMMENT '0- Inativo; 1 - Ativo; ',
  `tipo_popup` int(11) DEFAULT '1' COMMENT '1 - Abre uma vez; 2 - Abre sempre',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `config`
--

INSERT INTO `config` (`id`, `dominio`, `url_metatags`, `manutencao`, `ips`, `imagem_popup`, `link_popup`, `ativo`, `tipo_popup`) VALUES
(1, 'www.teste.pt/', 'www.teste.pt/', 0, '', NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `config_imagens`
--

CREATE TABLE IF NOT EXISTS `config_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL COMMENT 'Pasta /imgs/',
  `imagem1` varchar(255) DEFAULT NULL COMMENT 'Imagem Zoom',
  `min_height1` varchar(255) DEFAULT NULL,
  `max_width1` varchar(255) DEFAULT NULL,
  `imagem2` varchar(255) DEFAULT NULL COMMENT 'Imagem Normal',
  `min_height2` varchar(255) DEFAULT NULL,
  `max_width2` varchar(255) DEFAULT NULL,
  `imagem3` varchar(255) DEFAULT NULL COMMENT 'Imagem List',
  `min_height3` varchar(255) DEFAULT NULL,
  `max_width3` varchar(255) DEFAULT NULL,
  `imagem4` varchar(255) DEFAULT NULL COMMENT 'Imagem Pequena',
  `min_height4` varchar(255) DEFAULT NULL,
  `max_width4` varchar(255) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`titulo`,`nome`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `config_imagens`
--

INSERT INTO `config_imagens` (`id`, `titulo`, `nome`, `imagem1`, `min_height1`, `max_width1`, `imagem2`, `min_height2`, `max_width2`, `imagem3`, `min_height3`, `max_width3`, `imagem4`, `min_height4`, `max_width4`, `ativo`) VALUES
(1, 'Produtos', 'produtos', '1500x1350', NULL, NULL, '450x405', NULL, NULL, '373x335', NULL, NULL, '100x90', NULL, NULL, 1),
(2, 'Banners', 'banners', '1920x600', NULL, NULL, '1000x600', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 'Paginas', 'paginas', '3840x650', '250', NULL, '2574x1240', NULL, NULL, '1260x620', NULL, NULL, NULL, NULL, NULL, 1),
(4, 'Noticias', 'noticias', '1000x350', NULL, NULL, '900x485', NULL, '506', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(5, 'Testemunhos', 'testemunhos', '373x276', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(6, 'Categorias', 'categorias', '1920x200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(7, 'Marcas', 'marcas', '1920x400', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(8, 'Destaques', 'destaques', '1920x400', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(9, 'Blog', 'blog', '1920x400', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(10, 'Equipa', 'equipa', '1920x400', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(11, 'Catalogos', 'catalogos', '1920x400', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(12, 'Imagens Topo', 'imagens_topo', '3840x900', '250', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(13, 'Contactos', 'contactos', '1920x600', '400', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(14, 'Área Reservada', 'area_reservada', '906x728', NULL, NULL, NULL, NULL, NULL, '785x450', NULL, NULL, NULL, NULL, NULL, 1),
(15, 'Blog', 'blog/posts', '540x500', NULL, NULL, '2880x1428', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `config_sessions`
--

CREATE TABLE IF NOT EXISTS `config_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `nome_session` varchar(255) NOT NULL,
  `query` varchar(1000) DEFAULT '*',
  `query2` varchar(1000) DEFAULT NULL,
  `query3` varchar(1000) DEFAULT NULL,
  `onlyRow` tinyint(4) DEFAULT '0' COMMENT '0: Não; 1: Sim',
  `geral` tinyint(4) DEFAULT '0',
  `refresh` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `indice` (`nome`,`nome_session`,`onlyRow`,`geral`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `config_sessions`
--

INSERT INTO `config_sessions` (`id`, `nome`, `nome_session`, `query`, `query2`, `query3`, `onlyRow`, `geral`, `refresh`) VALUES
(1, 'Banners', 'banners', 'SELECT * FROM banners_h#extensao# WHERE visivel=''1'' AND ((datai<=''#data#'' OR datai IS NULL OR datai='''') AND (dataf>=''#data#'' OR dataf IS NULL OR dataf='''')) ORDER BY ordem ASC, rand()', NULL, NULL, 0, 0, '2017-07-14 07:29:04'),
(2, 'Redes Sociais', 'redes', 'SELECT * FROM redes_sociais WHERE visivel = ''1'' AND link IS NOT NULL AND link!='''' ORDER BY ordem ASC', NULL, NULL, 0, 1, '2017-07-14 07:29:04'),
(3, 'Linguas', 'linguas', 'SELECT * FROM linguas WHERE visivel = ''1'' AND ativo = ''1'' ORDER BY id ASC', NULL, NULL, 0, 1, '2017-07-14 07:29:04'),
(4, 'Noticias', 'noticias', 'SELECT * FROM noticias#extensao# WHERE visivel = 1 ORDER BY ordem ASC, data DESC, id DESC', 'SELECT * FROM noticias_imagens WHERE visivel = ''1'' AND id_peca = #id# ORDER BY ordem ASC', NULL, 0, 0, '2017-07-26 09:57:21'),
(5, 'Contactos', 'contactos', 'SELECT * FROM contactos#extensao# WHERE id = 1', 'SELECT * FROM contactos_locais#extensao# WHERE visivel = 1 ORDER BY ordem ASC', NULL, 1, 1, '2017-07-14 07:29:04'),
(6, 'Paises', 'paises', 'SELECT * FROM paises WHERE visivel=1 ORDER BY nome ASC', NULL, NULL, 0, 1, '2017-07-14 07:29:04'),
(7, 'Paginas', 'paginas', 'SELECT * FROM paginas#extensao# WHERE visivel = 1 ORDER BY ordem ASC', 'SELECT * FROM paginas_blocos#extensao# WHERE pagina = #id# AND visivel = ''1'' ORDER BY ordem ASC', 'SELECT * FROM paginas_blocos_imgs WHERE visivel = ''1'' AND bloco = #id# ORDER BY ordem ASC', 0, 1, '2017-09-21 10:56:45'),
(8, 'Metatags', 'metatags', 'SELECT * FROM metatags#extensao# WHERE visivel=1 ORDER BY ordem ASC', NULL, NULL, 0, 1, '2017-11-23 15:51:30'),
(9, 'Faqs', 'faqs', 'SELECT cats.* FROM faqs_categorias#extensao# AS cats LEFT JOIN faqs#extensao# AS faqs ON cats.id = faqs.categoria WHERE cats.visivel = 1 AND faqs.visivel = 1 ORDER BY cats.ordem ASC', 'SELECT * FROM faqs#extensao# WHERE visivel = 1 AND categoria = #id# ORDER BY ordem ASC', NULL, 0, 0, '2017-12-07 14:41:30'),
(10, 'Categorias', 'categorias', 'SELECT cat.* FROM l_categorias#extensao# AS cat, l_pecas#extensao# AS pecas WHERE cat.visivel = ''1'' AND cat.cat_mae = 0 AND pecas.visivel=''1''  GROUP BY cat.id ORDER BY cat.ordem ASC, cat.id ASC', 'SELECT cat.* FROM l_categorias#extensao# AS cat, l_pecas#extensao# AS pecas WHERE cat.visivel = ''1'' AND cat_mae = #id# AND pecas.visivel=''1'' GROUP BY cat.id ORDER BY cat.ordem ASC, cat.id ASC', 'SELECT cat.* FROM l_categorias#extensao# AS cat, l_pecas#extensao# AS pecas WHERE cat.visivel = ''1'' AND cat_mae = #id# AND pecas.visivel=''1'' GROUP BY cat.id ORDER BY cat.ordem ASC, cat.id ASC', 0, 1, '2018-09-20 09:37:16'),
(11, 'Paginas fixas menu', 'paginas_fixas', 'SELECT * FROM paginas#extensao# WHERE visivel = 1 AND fixo = 1 AND mostrar_menu = 0 ORDER BY ordem ASC', 'SELECT * FROM paginas_blocos#extensao# WHERE pagina = #id# AND visivel = ''1'' ORDER BY ordem ASC', 'SELECT * FROM paginas_blocos_imgs WHERE visivel = ''1'' AND bloco = #id# ORDER BY ordem ASC', 0, 1, '2019-04-03 14:54:20'),
(12, 'Paginas menu', 'paginas_menu', 'SELECT * FROM paginas#extensao# WHERE visivel = 1 AND mostrar_menu = 1 ORDER BY ordem ASC', 'SELECT * FROM paginas_blocos#extensao# WHERE pagina = #id# AND visivel = ''1'' ORDER BY ordem ASC', 'SELECT * FROM paginas_blocos_imgs WHERE visivel = ''1'' AND bloco = #id# ORDER BY ordem ASC', 0, 1, '2019-04-03 12:45:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contactos_locais_pt`
--

CREATE TABLE IF NOT EXISTS `contactos_locais_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `texto` text,
  `gps` varchar(250) DEFAULT NULL,
  `link_google_maps` varchar(250) DEFAULT NULL,
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  `footer` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indice` (`ordem`,`visivel`,`footer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contactos_pt`
--

CREATE TABLE IF NOT EXISTS `contactos_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` text,
  `mapa` text,
  `email` varchar(250) DEFAULT NULL,
  `telefone` varchar(250) DEFAULT NULL,
  `gps` varchar(100) DEFAULT NULL,
  `link_google_maps` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contactos_pt`
--

INSERT INTO `contactos_pt` (`id`, `texto`, `mapa`, `email`, `telefone`, `gps`, `link_google_maps`) VALUES
(1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `destaques_pt`
--

CREATE TABLE IF NOT EXISTS `destaques_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `target` varchar(50) DEFAULT '0',
  `texto` text,
  `imagem1` text,
  `imagem2` text,
  `texto_botao` varchar(100) DEFAULT NULL,
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`ordem`,`visivel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipa_pt`
--

CREATE TABLE IF NOT EXISTS `equipa_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `funcao` varchar(255) DEFAULT NULL,
  `descricao` text,
  `imagem1` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`ordem`,`visivel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `homepage_pt`
--

CREATE TABLE IF NOT EXISTS `homepage_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo1` varchar(250) DEFAULT NULL,
  `subtitulo1` varchar(255) DEFAULT NULL,
  `icone1` varchar(255) DEFAULT NULL,
  `titulo2` varchar(255) DEFAULT NULL,
  `subtitulo2` varchar(255) DEFAULT NULL,
  `icone2` varchar(255) DEFAULT NULL,
  `titulo3` varchar(255) DEFAULT NULL,
  `subtitulo3` varchar(255) DEFAULT NULL,
  `icone3` varchar(255) DEFAULT NULL,
  `imagem_news` varchar(255) DEFAULT NULL,
  `texto_news` text,
  `imagem_form` varchar(255) DEFAULT NULL,
  `texto_form` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `homepage_pt`
--

INSERT INTO `homepage_pt` (`id`, `titulo1`, `subtitulo1`, `icone1`, `titulo2`, `subtitulo2`, `icone2`, `titulo3`, `subtitulo3`, `icone3`, `imagem_news`, `texto_news`, `imagem_form`, `texto_form`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens_topo`
--

CREATE TABLE IF NOT EXISTS `imagens_topo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faqs` varchar(255) DEFAULT NULL,
  `faqs_masc` tinyint(4) NOT NULL DEFAULT '0',
  `contactos` varchar(255) DEFAULT NULL,
  `contactos_masc` tinyint(4) NOT NULL DEFAULT '0',
  `noticias` varchar(255) DEFAULT NULL,
  `noticias_masc` tinyint(4) NOT NULL DEFAULT '0',
  `blog` varchar(255) DEFAULT NULL,
  `blog_masc` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `imagens_topo`
--

INSERT INTO `imagens_topo` (`id`, `faqs`, `faqs_masc`, `contactos`, `contactos_masc`, `noticias`, `noticias_masc`, `blog`, `blog_masc`) VALUES
(1, '', 0, '', 0, '', 0, '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `linguas`
--

CREATE TABLE IF NOT EXISTS `linguas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sufixo` varchar(5) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `nome_para_loja` varchar(250) DEFAULT NULL,
  `nome_para_noticias` varchar(250) DEFAULT NULL,
  `visivel` tinyint(4) DEFAULT '1',
  `ativo` tinyint(4) DEFAULT '1',
  `consola` tinyint(4) DEFAULT '0',
  `ordem` int(11) DEFAULT '99',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `indice` (`visivel`,`ativo`,`consola`,`ordem`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `linguas`
--

INSERT INTO `linguas` (`id`, `sufixo`, `nome`, `nome_para_loja`, `nome_para_noticias`, `visivel`, `ativo`, `consola`, `ordem`) VALUES
(1, 'pt', 'Português', 'loja', 'noticias', 1, 1, 1, 99);

-- --------------------------------------------------------

--
-- Estrutura da tabela `menus_paginas_pt`
--

CREATE TABLE IF NOT EXISTS `menus_paginas_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `metatags_pt`
--

CREATE TABLE IF NOT EXISTS `metatags_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagina` varchar(250) DEFAULT NULL,
  `title` text,
  `description` text,
  `keywords` text,
  `blog` int(11) DEFAULT '0',
  `url` varchar(250) DEFAULT NULL,
  `ficheiro` varchar(250) DEFAULT NULL,
  `visivel` tinyint(4) DEFAULT '1',
  `ordem` int(11) DEFAULT '99',
  `editar` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`ordem`,`visivel`,`editar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `metatags_pt`
--

INSERT INTO `metatags_pt` (`id`, `pagina`, `title`, `description`, `keywords`, `blog`, `url`, `ficheiro`, `visivel`, `ordem`, `editar`) VALUES
(1, 'Homepage', NULL, NULL, NULL, 0, 'index.php', 'index.php', 1, 1, 0),
(2, 'Contactos', 'Contactos', 'Contactos', 'Contactos', 0, 'contactos.php', 'contactos.php', 1, 99, 1),
(3, 'Produtos', 'Produtos', 'Produtos', 'Produtos', 0, 'produtos.php', 'produtos.php', 1, 99, 1),
(4, 'Novidades', 'Novidades', 'Novidades', 'Novidades', 0, '', '', 1, 99, 1),
(5, 'Promoções', 'Promoções', 'Promoções', 'Promoções', 0, '', '', 1, 99, 1),
(6, 'Loja Online', 'Loja Online', 'Loja Online', 'Loja Online', 0, 'loja', '', 1, 99, 1),
(7, 'Faqs', 'Faqs', 'Faqs', 'Faqs', 0, 'faqs.php', 'faqs.php', 1, 99, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `news_emails`
--

CREATE TABLE IF NOT EXISTS `news_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `empresa` varchar(255) DEFAULT NULL,
  `cargo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `data` datetime DEFAULT NULL,
  `origem` varchar(255) DEFAULT NULL,
  `aceita` tinyint(4) DEFAULT '0',
  `data_remocao` datetime DEFAULT NULL,
  `origem_remocao` varchar(255) DEFAULT NULL,
  `visivel` tinyint(4) NOT NULL,
  `codigo` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `indice` (`email`,`visivel`,`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `news_emails_listas`
--

CREATE TABLE IF NOT EXISTS `news_emails_listas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` int(11) DEFAULT '0',
  `lista` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indice` (`email`,`lista`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `news_emails_temp`
--

CREATE TABLE IF NOT EXISTS `news_emails_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `data` datetime DEFAULT NULL,
  `origem` varchar(255) DEFAULT NULL,
  `aceita` tinyint(4) DEFAULT '0',
  `codigo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `indice` (`email`,`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `news_listas`
--

CREATE TABLE IF NOT EXISTS `news_listas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '99',
  `permite_editar` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`ordem`,`permite_editar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `news_listas`
--

INSERT INTO `news_listas` (`id`, `nome`, `ordem`, `permite_editar`) VALUES
(1, 'Website', 1, 0),
(2, 'Clientes', 2, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias_imagens`
--

CREATE TABLE IF NOT EXISTS `noticias_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_peca` int(11) DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `imagem1` varchar(250) DEFAULT NULL,
  `imagem2` varchar(250) DEFAULT NULL,
  `legenda` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`id_peca`,`id_tipo`,`ordem`,`visivel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias_pt`
--

CREATE TABLE IF NOT EXISTS `noticias_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `resumo` text,
  `descricao` text,
  `imagem1` varchar(350) DEFAULT NULL,
  `imagem2` varchar(350) DEFAULT NULL,
  `imagem3` varchar(350) DEFAULT NULL,
  `video` text,
  `ficheiro` varchar(250) DEFAULT NULL,
  `destaque` tinyint(4) DEFAULT '0',
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '0',
  `url` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `keywords` text,
  PRIMARY KEY (`id`),
  KEY `indice` (`data`,`destaque`,`ordem`,`visivel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificacoes_pt`
--

CREATE TABLE IF NOT EXISTS `notificacoes_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `email` text,
  `email2` text,
  `email3` text,
  `assunto` varchar(255) DEFAULT NULL,
  `assunto_cliente` varchar(255) DEFAULT NULL,
  `resposta` text,
  `sucesso` varchar(255) DEFAULT NULL,
  `resposta_editavel` tinyint(4) DEFAULT '1' COMMENT '0=Não editavel; 1=editavel; ',
  `visivel` tinyint(4) DEFAULT '1',
  `ordem` int(11) DEFAULT '99',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `notificacoes_pt`
--

INSERT INTO `notificacoes_pt` (`id`, `nome`, `email`, `email2`, `email3`, `assunto`, `assunto_cliente`, `resposta`, `sucesso`, `resposta_editavel`, `visivel`, `ordem`) VALUES
(1, 'Formulário de contacto', '	carlossampaio@netgocio.pt', '', '', 'Novo pedido de contacto', 'Pedido de contacto', 'Caro <strong>#nome#</strong>,<br />\r\nO seu contacto foi recebido com sucesso.<br />\r\n<br />\r\nIremos responder assim que possivel.<br />\r\n<br />\r\n<br />\r\nObrigado', NULL, 1, 1, 10),
(6, 'Formulário de contacto das páginas', '	carlossampaio@netgocio.pt', '', '', 'Novo pedido de informações', 'Novo pedido de informações', 'Caro <strong>#nome#</strong>,<br />\r\nO seu contacto foi recebido com sucesso.<br />\r\n<br />\r\nIremos responder assim que possivel.<br />\r\n<br />\r\n<br />\r\nObrigado', NULL, 1, 1, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas_blocos_ficheiros_pt`
--

CREATE TABLE IF NOT EXISTS `paginas_blocos_ficheiros_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bloco` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `ficheiro` varchar(255) DEFAULT NULL,
  `tamanho` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`bloco`,`ordem`,`visivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `paginas_blocos_ficheiros_pt`
--

INSERT INTO `paginas_blocos_ficheiros_pt` (`id`, `bloco`, `nome`, `ficheiro`, `tamanho`, `ordem`, `visivel`) VALUES
(1, 34, 'Lorem ipsum dolor sit amet 2018', 'newsletter_retina.pdf', '1,2 MB', 99, 1),
(2, 43, NULL, 'Frutiger_Adrian_Signs_and_Symbols_Their_Design_and_Meaning.pdf', '38,5 MB', 99, 1),
(3, 34, 'm ipsum dolor sit amet 2018', 'Frutiger_Adrian_Signs_and_Symbols_Their_Design_and_Meaning_1.pdf', '38,5 MB', 99, 1),
(4, 34, 'm ipsum dolor sit amet 2018', 'newsletter_retina_1.pdf', '1,2 MB', 99, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas_blocos_imgs`
--

CREATE TABLE IF NOT EXISTS `paginas_blocos_imgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bloco` int(11) DEFAULT NULL,
  `coluna` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `imagem1` varchar(255) DEFAULT NULL,
  `tipo` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - Imagem / 1 - Vídeo Upload / 2 - Link Vídeo',
  `proporcao_video` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - 16:9 / 2 - 4:3',
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`bloco`,`ordem`,`visivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Extraindo dados da tabela `paginas_blocos_imgs`
--

INSERT INTO `paginas_blocos_imgs` (`id`, `bloco`, `coluna`, `link`, `imagem1`, `tipo`, `proporcao_video`, `ordem`, `visivel`) VALUES
(29, 25, NULL, '', 'IVOPRODUCTS_09.jpg', 0, 1, 3, 1),
(30, 25, NULL, '', 'IVOPRODUCTS_10.jpg', 0, 1, 2, 1),
(32, 26, NULL, NULL, 'https://www.youtube.com/embed/vlDzYIIOYmM', 2, 1, 99, 1),
(34, 26, NULL, NULL, 'SampleVideo_1280x720_1mb.mp4', 1, 1, 99, 1),
(35, 27, NULL, NULL, 'IVOPRODUCTS_07.jpg', 0, 1, 99, 1),
(36, 30, 2, NULL, 'https://www.youtube.com/embed/vlDzYIIOYmM', 2, 1, 99, 1),
(39, 24, NULL, '', 'banner01.jpg', 0, 1, 99, 1),
(40, 25, NULL, NULL, 'SampleVideo_1280x720_1mb_1.mp4', 1, 1, 99, 1),
(41, 30, 1, '', '040925_1_1336_IVOPRODUCTS_07.jpg', 0, 1, 99, 1),
(42, 36, NULL, NULL, 'imagem_de_teste.png', 0, 1, 99, 1),
(43, 37, NULL, NULL, 'imagem_de_teste_1.png', 0, 1, 99, 1),
(44, 38, NULL, '', 'imagem_de_teste_2.png', 0, 1, 2, 1),
(45, 38, NULL, '', 'imagem_de_teste_2_1.png', 0, 1, 1, 1),
(46, 39, NULL, NULL, 'https://youtu.be/jJY0hUwQ0_8', 2, 1, 99, 1),
(47, 41, 2, '', 'imagem_de_teste_3.png', 0, 1, 99, 1),
(48, 41, 1, '', 'imagem_de_teste_2_2.png', 0, 1, 99, 1),
(49, 24, NULL, NULL, 'https://youtu.be/G9KDqfpCgws', 2, 1, 99, 1),
(50, 24, NULL, '', 'The_mirage.jpg', 0, 1, 99, 1),
(51, 31, NULL, NULL, 'Destaque_04.jpg', 0, 1, 99, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas_blocos_pt`
--

CREATE TABLE IF NOT EXISTS `paginas_blocos_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagina` int(11) DEFAULT NULL,
  `tem_fundo` tinyint(4) NOT NULL DEFAULT '0',
  `tipo_fundo` tinyint(4) NOT NULL DEFAULT '1',
  `cor_fundo` varchar(50) DEFAULT NULL,
  `imagem_fundo` varchar(255) DEFAULT NULL,
  `mascara_fundo` tinyint(4) DEFAULT '1',
  `tipo_imagens` tinyint(4) DEFAULT '1' COMMENT '1 - Galeria ; 0 - Sem Galeria',
  `esp_imagens` tinyint(4) DEFAULT '0' COMMENT 'Espaçamento entre as imagens (px)',
  `largura_imgs` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - Largura Máxima / 2 - Largura específica',
  `valor_largura_imgs` int(11) DEFAULT NULL,
  `largura_texto` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - Largura Máxima / 2 - Largura específica',
  `valor_largura_texto` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `titulo1` varchar(255) DEFAULT NULL,
  `texto` text,
  `titulo2` varchar(255) DEFAULT NULL,
  `texto2` text,
  `titulo3` varchar(255) DEFAULT NULL,
  `texto3` text,
  `video` varchar(500) DEFAULT NULL,
  `tipo` tinyint(4) DEFAULT NULL,
  `tipo_galeria` tinyint(4) DEFAULT '1' COMMENT '1 - Galeria; 0 - Vídeo',
  `fullscreen` tinyint(4) DEFAULT '0' COMMENT '1 - Galeria ocupa 100% largura',
  `texto_contorna` tinyint(4) DEFAULT '0',
  `orientacao` tinyint(4) DEFAULT '0',
  `colunas` int(11) DEFAULT '1',
  `link1` varchar(255) DEFAULT NULL,
  `target1` varchar(255) DEFAULT NULL,
  `texto_botao1` varchar(255) DEFAULT NULL,
  `link2` varchar(255) DEFAULT NULL,
  `target2` varchar(255) DEFAULT NULL,
  `texto_botao2` varchar(255) DEFAULT NULL,
  `link3` varchar(255) DEFAULT NULL,
  `target3` varchar(255) DEFAULT NULL,
  `texto_botao3` varchar(255) DEFAULT NULL,
  `mapa` text,
  `visivel` tinyint(4) DEFAULT '0',
  `ordem` int(11) DEFAULT '99',
  PRIMARY KEY (`id`),
  KEY `indice` (`pagina`,`tipo_imagens`,`tipo`,`tipo_galeria`,`fullscreen`,`texto_contorna`,`orientacao`,`colunas`,`ordem`,`visivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Extraindo dados da tabela `paginas_blocos_pt`
--

INSERT INTO `paginas_blocos_pt` (`id`, `pagina`, `tem_fundo`, `tipo_fundo`, `cor_fundo`, `imagem_fundo`, `mascara_fundo`, `tipo_imagens`, `esp_imagens`, `largura_imgs`, `valor_largura_imgs`, `largura_texto`, `valor_largura_texto`, `nome`, `titulo`, `titulo1`, `texto`, `titulo2`, `texto2`, `titulo3`, `texto3`, `video`, `tipo`, `tipo_galeria`, `fullscreen`, `texto_contorna`, `orientacao`, `colunas`, `link1`, `target1`, `texto_botao1`, `link2`, `target2`, `texto_botao2`, `link3`, `target3`, `texto_botao3`, `mapa`, `visivel`, `ordem`) VALUES
(22, 2, 0, 1, NULL, NULL, 1, NULL, NULL, 1, NULL, 1, NULL, 'Texto', '', '', 'Este website utiliza <em>cookies</em>&nbsp;para melhorar o seu servi&ccedil;o.<br />\n<br />\n<strong>1. O que s&atilde;o&nbsp;<em>cookies</em></strong>?<br />\nOs&nbsp;<em>cookies</em>&nbsp;s&atilde;o pequenos ficheiros de texto que um&nbsp;web<em>site</em>, ao ser visitado pelo utilizador, coloca no seu computador ou no seu dispositivo m&oacute;vel atrav&eacute;s do navegador de internet (browser). A coloca&ccedil;&atilde;o de&nbsp;<em>cookies</em>&nbsp;ajudar&aacute; o&nbsp;<em>site</em>&nbsp;a reconhecer o seu dispositivo na pr&oacute;xima vez que o utilizador o visita.&nbsp;<br />\n<br />\nUsamos o termo&nbsp;<em>cookies</em>&nbsp;nesta pol&iacute;tica para referir todos os ficheiros que recolhem informa&ccedil;&otilde;es desta forma.&nbsp;.Alguns cookies s&atilde;o essenciais para garantir as funcionalidades disponibilizadas, enquanto outros s&atilde;o destinadas a melhorar o desempenho e a experi&ecirc;ncia do utilizador.&nbsp;N&atilde;o dever&aacute; continuar a aceder ao nosso <em>website</em> ap&oacute;s o alerta sobre os cookies, se n&atilde;o concordar com a sua utiliza&ccedil;&atilde;o.<br />\n&nbsp;<br />\n<strong>Que tipo de c</strong><em><strong>ookies</strong></em><strong>&nbsp;utilizamos</strong>?<br />\n&nbsp;<br />\n<table border="0" cellpadding="0" cellspacing="0" style="width:596px"><thead><tr><td class="cor_op_1">ORIGEM</td><td class="cor_op_05">NOME COOKIE</td><td class="cor_op_1">PROP&Oacute;SITO</td><td class="cor_op_05">EXPIRA</td></tr></thead><tbody><tr class="cor3_op"><td class="cor2_op_05" rowspan="2">clinicacentraldaareosa</td><td class="cor2_op_1">PHPSESSID</td><td class="cor2_op_05">&Eacute; utilizado pela linguagem PHP para permitir que as vari&aacute;veis de sess&atilde;o sejam guardadas no servidor web. Este cookie &eacute; essencial para o funcionamento do website.</td><td class="cor2_op_1">Quando o browser &eacute; fechado</td></tr><tr class="cor3_op"><td class="cor2_op_1">allowCookies</td><td class="cor2_op_05">Utilizado para controlar a aceita&ccedil;&atilde;o do utilizador da pol&iacute;tica de cookies do site</td><td class="cor2_op_1">Ap&oacute;s 1 ano</td></tr><tr><td class="cor2_op_05">AddThis</td><td class="cor2_op_1">__atuvc</td><td class="cor2_op_05">Criado e lido pelo AddThis, site JavaScript para a partilha nas redes sociais. Guarda o n&uacute;mero de partilhas de uma p&aacute;gina.</td><td class="cor2_op_1">Ap&oacute;s 2 anos</td></tr><tr class="cor3_op"><td class="cor2_op_05" rowspan="4">Google<br />(<a href="https://developers.google.com/analytics/devguides/collection/gtagjs/cookie-usage" target="_blank">Saber mais</a>)</td><td class="cor2_op_1">_ga</td><td class="cor2_op_05">Utilizado para distinguir os utilizadores.</td><td class="cor2_op_1">Ap&oacute;s 2 anos</td></tr><tr class="cor3_op"><td class="cor2_op_1">_gat</td><td class="cor2_op_05">Utilizado para acelerar a percentagem de pedidos.</td><td class="cor2_op_1">Ap&oacute;s 1 minuto</td></tr><td class="cor2_op_1">gat_gtag_UA</td><td class="cor2_op_05">Cont&eacute;m informa&ccedil;&otilde;es relacionadas com campanhas para o utilizador. Ao vincular suas contas do Google Analytics e do Google AdWords, as tags de convers&atilde;o de site do Google AdWords analisar&atilde;o esse cookie, a menos que esteja inativo.</td><td class="cor2_op_1">Ap&oacute;s 1 minuto</td></tr><tr class="cor3_op"><td class="cor2_op_1">gid</td><td class="cor2_op_05">Utilizado para distinguir os utilizadores.</td><td class="cor2_op_1">Ap&oacute;s 1 dia</td></tr><tr><td class="cor2_op_05" rowspan="7">Facebook<br />(<a href="https://www.facebook.com/policies/cookies/" target="_blank">Saber mais</a>)</td><td class="cor2_op_1">c_user</td><td class="cor2_op_05">&Eacute; utilizado na integra&ccedil;&atilde;o do Facebook e na partilha de conte&uacute;dos.</td><td class="cor2_op_1">Ap&oacute;s 3 meses</td></tr><tr><td class="cor2_op_1">datr</td><td class="cor2_op_05">&Eacute; utilizado na seguran&ccedil;a e integridade do website. Cont&eacute;m identifica&ccedil;&atilde;o do browser.</td><td class="cor2_op_1">Ap&oacute;s 2 anos</td></tr><tr><td class="cor2_op_1">fr</td><td class="cor2_op_05">&Eacute; utilizado para fins publicit&aacute;rios. Cont&eacute;m informa&ccedil;&atilde;o cifrada da identifica&ccedil;&atilde;o do Facebook e do browser.</td><td class="cor2_op_1">Ap&oacute;s 3 meses</td></tr><tr><td class="cor2_op_1">presence</td><td class="cor2_op_05">Cont&eacute;m informa&ccedil;&atilde;o do estado do chat</td><td class="cor2_op_1">Quando o browser &eacute; fechado</td></tr><tr><td class="cor2_op_1">sb</td><td class="cor2_op_05">-</td><td class="cor2_op_1">Ap&oacute;s 2 anos</td></tr><tr><td class="cor2_op_1">wd</td><td class="cor2_op_05">Cont&eacute;m informa&ccedil;&atilde;o da dimens&atilde;o da janela do browser</td><td class="cor2_op_1">Ap&oacute;s 7 dias</td></tr><tr><td class="cor2_op_1">xs</td><td class="cor2_op_05">Cont&eacute;m n&uacute;mero de sess&atilde;o e&nbsp;<em>secret</em></td><td class="cor2_op_1">Ap&oacute;s 3 meses</td></tr></tbody></table>\n&nbsp;<br />\n&nbsp;<br />\n&nbsp;<br />\n<strong>2. Aceitar/recusar cookies</strong><br />\nPoder&aacute; a qualquer momento optar por aceitar ou recusar a instala&ccedil;&atilde;o de <em>cookies </em>no seu terminal, configurando o software de navega&ccedil;&atilde;o.&nbsp;<br />\n<br />\n<strong>2.1 Se aceitar cookies</strong>&nbsp;<br />\nO registo de uma cookie no seu terminal depende da sua vontade. Em qualquer momento, poder&aacute; exprimir e modificar a sua escolha gratuitamente atrav&eacute;s das op&ccedil;&otilde;es disponibilizadas pelo seu software de navega&ccedil;&atilde;o.&nbsp;<br />\n<br />\nSe no software de navega&ccedil;&atilde;o que utiliza tiver aceitado a grava&ccedil;&atilde;o de cookies no seu terminal, as cookies integradas nas p&aacute;ginas e conte&uacute;dos que tiver consultado poder&atilde;o ficar temporariamente armazenadas num espa&ccedil;o espec&iacute;fico do seu terminal. Nesse local, ser&atilde;o leg&iacute;veis apenas pelo emissor das mesmas.&nbsp;<br />\n<br />\n<strong>2.2 Qual &eacute; o interesse de aceitar cookies?&nbsp;</strong><br />\nQuando visitar website www.teste.pt, estas cookies registar&atilde;o determinadas informa&ccedil;&otilde;es que est&atilde;o armazenadas no seu terminal. Estas informa&ccedil;&otilde;es servem, nomeadamente, para lhe prop&ocirc;r produtos em fun&ccedil;&atilde;o dos artigos que j&aacute; seleccionou aquando de suas visitas anteriores, e permitem-lhe assim beneficiar de uma melhor navega&ccedil;&atilde;o no nosso Site.&nbsp;<br />\n<br />\n<strong>3.2 Se recusar <em>cookies&nbsp;</em></strong><br />\nQuando recusa <em>cookies,</em> n&oacute;s instalamos contudo uma <em>cookie</em> de <em>&quot;recusa</em>&quot;. Esta <em>cookie </em>permite-nos memorizar a sua escolha, de modo a evitar que lhe perguntemos a cada visita se deseja aceitar ou recusar cookies.<br />\n&nbsp;<br />\n<strong>3.3 Configura&ccedil;&atilde;o do seu navegador</strong><br />\nA configura&ccedil;&atilde;o da gest&atilde;o das cookies depende de cada navegador e est&aacute; presente no menu de ajuda do seu navegador.<br /><br />\n<strong>No Internet Explorer</strong><br />\nPara personalizar as defini&ccedil;&otilde;es relativas &agrave;s cookies para uma p&aacute;gina web<br />\nNo Internet Explorer, clique no bot&atilde;o Ferramentas e depois em Op&ccedil;&otilde;es da Internet.<br />\nClique no separador Privacidade e depois em Sites.<br />\nNo espa&ccedil;o Endere&ccedil;o do Web site, insira o endere&ccedil;o completo (URL) da p&aacute;gina web cujas defini&ccedil;&otilde;es de privacidade deseja personalizar.&nbsp;<br />\nPor exemplo, http://www.microsoft.com.<br />\nPara autorizar o registo de cookies da p&aacute;gina web especificada no seu computador, clique em Permitir. Para proibir o registo de cookies da p&aacute;gina web especificada no seu computador, clique em Bloquear.<br />\nRepita as etapas 3 e 4 para cada p&aacute;gina web que queira bloquear ou permitir. Quando terminar, clique em OK duas vezes.<br />\n<br />\n<strong>No Safari</strong><br />\nV&aacute; a Prefer&ecirc;ncias, clique no painel Privacidade e, de seguida, Gerir as Cookies.<br />\n<br />\n<strong>No Chrome</strong><br />\nClique no menu do Chrome que est&aacute; situado na barra de ferramentas do navegador.<br />\nSeleccione Defini&ccedil;&otilde;es.<br />\nClique em Mostrar defini&ccedil;&otilde;es avan&ccedil;adas.<br />\nNa sec&ccedil;&atilde;o &quot;Privacidade&quot;, clique no bot&atilde;o Defini&ccedil;&otilde;es de conte&uacute;do.<br />\nNa sec&ccedil;&atilde;o &quot;Cookies&quot; poder&aacute; modificar as defini&ccedil;&otilde;es seguintes:<br />\nEliminar cookies&nbsp;<br />\nBloquear cookies por predefini&ccedil;&atilde;o&nbsp;<br />\nPermitir cookies por predefini&ccedil;&atilde;o<br />\nCriar excep&ccedil;&otilde;es para cookies de Websites ou dom&iacute;nios espec&iacute;ficos.<br />\n<br />\n<strong>No Firefox</strong><br />\nClique no bot&atilde;o Ferramentas e depois em Op&ccedil;&otilde;es.<br />\nClique no separador Privacidade.<br />\nNo Hist&oacute;rico, seleccione &quot;Usar defini&ccedil;&otilde;es personalizadas para o hist&oacute;rico.&quot;.<br />\nAssinalar a quadr&iacute;cula &quot;aceitar cookies&quot; e clicar em excep&ccedil;&otilde;es para escolher os s&iacute;tios web que ter&atilde;o ou n&atilde;o ter&atilde;o permiss&atilde;o para instalar cookies no seu terminal.<br />\n<br />\n<strong>No Opera</strong><br />\nPrefer&ecirc;ncias &gt; Avan&ccedil;ado&gt; Cookies&nbsp;<br />\nAs prefer&ecirc;ncias de cookies permitem-lhe controlar a forma como o Opera gere as cookies. A configura&ccedil;&atilde;o por defeito &eacute; aceitar todas as cookies.<br />\nAceitar cookies&nbsp;<br />\n- Todas as cookies s&atilde;o aceites (por defeito)<br />\nAceitar cookies apenas do site que visito&nbsp;<br />\n- As cookies de terceiros, de um dom&iacute;nio exterior &agrave;quele que est&aacute; a visitar, ser&atilde;o recusadas<br />\nNunca aceitar cookies&nbsp;<br />\n- Todas as cookies ser&atilde;o recusadas<br />\n&nbsp;<br />\n<strong>4. Atualiza&ccedil;&atilde;o da Notifica&ccedil;&atilde;o Legal</strong><br />A clinicacentraldaareosa reserva-se o direito de fazer quaisquer altera&ccedil;&otilde;es ou corre&ccedil;&otilde;es a esta notifica&ccedil;&atilde;o de cookies. Por favor consulte esta p&aacute;gina regularmente de modo a rever a informa&ccedil;&atilde;o e verificar se existe alguma atualiza&ccedil;&atilde;o.<br /><br />\nEsta notifica&ccedil;&atilde;o foi atualizada em 3 de Maio de 2018', '', '', '', '', NULL, 2, NULL, NULL, NULL, NULL, 1, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(23, 4, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'RAL e RLL', '', '', '<strong>RAL</strong><br />\r\n<br />\r\nEm caso de lit&iacute;gio o consumidor pode recorrer a uma entidade de Resolu&ccedil;&atilde;o Alternativa de Lit&iacute;gios de Consumo (RAL). As entidades de Resolu&ccedil;&atilde;o Alternativa de Lit&iacute;gios de Consumo (RAL) s&atilde;o as entidades autorizadas a efetuar a media&ccedil;&atilde;o, concilia&ccedil;&atilde;o e arbitragem de lit&iacute;gios de consumo em Portugal que estejam inscritas na lista de entidades RAL prevista pela Lei n.&ordm; 144/2015.<br />\r\nClique em&nbsp;<a href="http://www.ipai.pt/fotos/gca/i006245_1459446712.pdf" target="_blank">www.ipai.pt/fotos/gca/i006245_1459446712.pdf</a><br />\r\n<br />\r\n<strong>RLL</strong><br />\r\n<br />\r\nO consumidor pode recorrer &agrave; plataforma europeia de resolu&ccedil;&atilde;o de lit&iacute;gios em linha dispon&iacute;vel em&nbsp;<a href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&amp;lng=PT" target="_blank">ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&amp;lng=PT</a>', '', '', '', '', NULL, 2, 0, 0, NULL, NULL, 1, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(24, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, 500, 2, 700, 'Estrutura Tipo 1 - Texto (título + texto + CTA) + imagem', 'Título (ajustar para aparecer a imagem por baixo do título e por cima do texto)', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;<br />\r\n<br />\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.', '', '', '', '', NULL, 1, 0, 0, 0, 2, NULL, 'https://netgocio.pt/', '_blank', 'CTA', '', '', '', '', '', '', NULL, 1, 1),
(25, 1, 0, 1, NULL, NULL, 1, 1, 0, 1, NULL, 1, NULL, 'Estrutura Tipo 1 - Texto (título + texto + CTA) + galeria de imagens', 'Título ', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;<br />\r\n<br />\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.', '', '', '', '', '', 1, 1, 0, 0, 3, NULL, 'https://netgocio.pt/', '_blank', 'CTA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2),
(26, 1, 0, 1, NULL, NULL, 1, 0, NULL, 2, 1000, 1, 0, 'Estrutura Tipo 1 - Texto (título + texto + CTA) + vídeo  (link youtube ou através de upload de vídeo)', 'Título', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;<br />\r\n<br />\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac.', '', '', '', '', NULL, 1, 0, 0, 0, 2, NULL, 'https://netgocio.pt/contactos.php', '_blank', 'CTA', '', '', '', '', '', '', NULL, 1, 3),
(27, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, 200, 1, NULL, 'Estrutura Tipo 1 - Full width ', 'Título', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;', '', '', '', '', NULL, 1, 0, 0, 0, 0, NULL, '', '', '', '', '', '', '', '', '', NULL, 1, 4),
(28, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 2, 700, 'Estrutura Tipo 2 - Título + Texto', 'Facas para profissionais', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae purus massa. Suspendisse potenti. Nulla facilisi. Nunc maximus tempor enim, vitae sodales mauris imperdiet ac. Integer auctor, dolor at ullamcorper egestas, orci lectus tincidunt nibh, vehicula eleifend lacus est ut libero. Nulla diam quam, luctus quis lobortis vitae, molestie non justo. Duis ut lacus ut dolor tristique posuere a at dolor. Sed pretium massa ut elit eleifend rutrum.', '', '', '', '', NULL, 2, 0, 0, NULL, NULL, 1, '', '', '', '', '', '', '', '', '', NULL, 1, 5),
(29, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Estrutura Tipo 2 - Título', 'Facas para profissionais', '', '', '', '', '', '', NULL, 2, 0, 0, NULL, NULL, 1, '', '', '', '', '', '', '', '', '', NULL, 1, 1),
(30, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 0, NULL, 'Estrutura Tipo 3 - Colunas com Imagem / Vídeo + Título + Texto e CTA (max: 3 colunas)', '', 'Título', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;', 'Título', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;', '', '', NULL, 3, 0, 0, NULL, NULL, 2, 'https://netgocio.pt/', '_blank', 'CTA', 'https://www.netgocio.pt/', '_blank', 'CTA 2', '', '', '', NULL, 1, 7),
(31, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Estrutura Tipo 1 - Texto à esquerda da Imagem', 'Título', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;', '', '', '', '', NULL, 1, 0, 0, 0, 0, NULL, 'https://netgocio.pt/', '_blank', 'CTA', '', '', '', '', '', '', NULL, 1, 8),
(32, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Estrutura Tipo 4 - Localização Google Maps', 'Título', '', 'IVO CUTELARIAS, LDA<br />\r\nP.O.BOX 1<br />\r\n2500-770 Santa Catarina CLD<br />\r\nPORTUGAL&nbsp;<br />\r\n<br />\r\nP. +351 262 925 340<br />\r\nF. +351 262 925 341<br />\r\n<br />\r\n<br />\r\nGPS. 39.445857, -9.015075<br />\r\n<br />\r\nCUSTOMER SUPPORT:<br />\r\nivocutelarias@ivocutelarias.com<br />\r\n<br />\r\nEXPORT SALES:<br />\r\ncomercial@ivocutelarias.com<br />\r\n<br />\r\nPORTUGAL SALES:<br />\r\ncomercial2@ivocutelarias.com<br />\r\n&nbsp;', '', '', '', '', NULL, 4, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3081.039941841125!2d-9.017377384234168!3d39.4458292220341!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd18b07331ab4fad%3A0xab6eb3248629e927!2sIvo+Cutelarias%2C+Lda.!5e0!3m2!1spt-PT!2spt!4v1538577851739" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>', 1, 9),
(33, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 0, NULL, 'Estrutura Tipo 5 - Formulário', 'Título', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit.&nbsp;<br />\r\nPraesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;', NULL, NULL, NULL, NULL, NULL, 5, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(34, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Estrutura do Tipo 8 - Upload de Ficheiros', 'Downloads', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rhoncus nisl in tincidunt suscipit. Praesent vehicula mauris sit amet euismod imperdiet lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;', '', '', '', '', NULL, 6, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, 1, 11),
(35, 1, 0, 2, '', NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Estrutura Timeline', 'Timeline', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(36, 1, 0, 1, NULL, NULL, 1, 0, NULL, 2, 700, 2, 700, 'Teste 1', 'Assinatura de 17 acordos são “passos concretos na relação” entre Portugal e China', '', 'Na Sala de Espelhos do Pal&aacute;cio Nacional de Queluz, e debaixo do olhar do Presidente da China, Xi Jinping, e do primeiro-ministro, Ant&oacute;nio Costa, foram rubricados 17 acordos envolvendo os dois Estados e empresas dos dois pa&iacute;ses.<br />\r\n<br />\r\nAqui, destaca-se o memorando de entendimento entre<a href="https://www.publico.pt/2018/12/04/mundo/noticia/marcelo-xi-querem-aprofundar-relacao-politica-portugal-china-1853529">&nbsp;Portugal e a China</a>&nbsp;sobre coopera&ccedil;&atilde;o no quadro da chamada &ldquo;nova rota da seda&rdquo; (one belt, one road), com uma refer&ecirc;ncia &agrave; mobilidade el&eacute;ctrica, mas inclui-se tamb&eacute;m outro para a exporta&ccedil;&atilde;o de uva de mesa e a cria&ccedil;&atilde;o de um projecto empresarial chin&ecirc;s em Matosinhos que dever&aacute; criar 150 empregos.', '', '', '', '', NULL, 1, 0, 0, 0, 3, NULL, '', '_blank', '', '', '', '', '', '', '', NULL, 1, 13),
(37, 1, 0, 1, NULL, NULL, 1, 0, NULL, 2, 340, 2, 500, 'Teste 2 - Contornar texto', 'Assinatura de 17 acordos são “passos concretos na relação” entre Portugal e China', '', 'Os acordos foram assinados esta quarta-feira no Pal&aacute;cio de Queluz pelo primeiro-ministro portugu&ecirc;s, Ant&oacute;nio Costa, e o presidente chin&ecirc;s, Xi Jinping.<span style="color:#d12c24"><strong>&nbsp;</strong></span>Na Sala de Espelhos do Pal&aacute;cio Nacional de Queluz, e debaixo do olhar do Presidente da China, Xi Jinping, e do primeiro-ministro, Ant&oacute;nio Costa, foram rubricados 17 acordos envolvendo os dois Estados e empresas dos dois pa&iacute;ses.&nbsp;<span style="color:#d12c24"><u><strong>(texto a contornar a imagem)</strong></u></span><br />\r\n<br />\r\nAqui, destaca-se o memorando de entendimento entre Portugal e China sobre coopera&ccedil;&atilde;o no quadro da chamada &ldquo;nova rota da seda&rdquo; (one belt, one road), com uma refer&ecirc;ncia &agrave; mobilidade el&eacute;ctrica, mas inclui-se tamb&eacute;m outro para a exporta&ccedil;&atilde;o de uva de mesa e a cria&ccedil;&atilde;o de um projecto empresarial chin&ecirc;s em Matosinhos que dever&aacute; criar 150 empregos.', '', '', '', '', NULL, 1, 0, 0, 0, 0, NULL, 'www.google.pt', '', 'Consultar notícia', '', '', '', '', '', '', NULL, 1, 15),
(38, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, 0, 'Teste 3', 'Assinatura de 17 acordos são “passos concretos na relação” entre Portugal e China', '', 'Na Sala de Espelhos do Pal&aacute;cio Nacional de Queluz, e debaixo do olhar do Presidente da China, Xi Jinping, e do primeiro-ministro, Ant&oacute;nio Costa, foram rubricados 17 acordos envolvendo os dois Estados e empresas dos dois pa&iacute;ses.<br />\r\n<br />\r\nAqui, destaca-se o memorando de entendimento entre<a href="https://www.publico.pt/2018/12/04/mundo/noticia/marcelo-xi-querem-aprofundar-relacao-politica-portugal-china-1853529">&nbsp;Portugal e a China</a>&nbsp;sobre coopera&ccedil;&atilde;o no quadro da chamada &ldquo;nova rota da seda&rdquo; (one belt, one road), com uma refer&ecirc;ncia &agrave; mobilidade el&eacute;ctrica, mas inclui-se tamb&eacute;m outro para a exporta&ccedil;&atilde;o de uva de mesa e a cria&ccedil;&atilde;o de um projecto empresarial chin&ecirc;s em Matosinhos que dever&aacute; criar 150 empregos.', '', '', '', '', NULL, 1, 0, 0, 0, 1, NULL, 'www.google.pt', '', 'Consultar notícia em destaque', '', '', '', '', '', '', NULL, 1, 14),
(39, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, 0, 1, NULL, 'Teste 4 - Video', 'Vídeo', '', 'Xi Jinping, Presidente da Rep&uacute;blica Popular da China, foi recebido por Marcelo Rebelo de Sousa, Presidente da Rep&uacute;blica Portuguesa, em Bel&eacute;m.', '', '', '', '', NULL, 1, 0, 0, 0, 0, NULL, '', '', '', '', '', '', '', '', '', NULL, 1, 16),
(40, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Teste 5 - 3 colunas', 'Assinatura de 17 acordos são “passos concretos na relação” entre Portugal e China', 'Assinatura dos acordos', 'Na Sala de Espelhos do Pal&aacute;cio Nacional de Queluz, e debaixo do olhar do Presidente da China, Xi Jinping, e do primeiro-ministro, Ant&oacute;nio Costa, foram rubricados 17 acordos envolvendo os dois Estados e empresas dos dois pa&iacute;ses.<br />\r\n<br />\r\nAqui, destaca-se o memorando de entendimento entre Portugal e China&nbsp;sobre coopera&ccedil;&atilde;o no quadro da chamada &ldquo;nova rota da seda&rdquo; (one belt, one road), com uma refer&ecirc;ncia &agrave; mobilidade el&eacute;ctrica, mas inclui-se tamb&eacute;m outro para a exporta&ccedil;&atilde;o de uva de mesa e a cria&ccedil;&atilde;o de um projecto empresarial chin&ecirc;s em Matosinhos que dever&aacute; criar 150 empregos.', 'Nova rota da seda', 'Ao todo, foram rubricados 17 acordos (e n&atilde;o 19 como chegou a ser avan&ccedil;ado), divididos entre Estado e empresas. Em primeiro lugar, surge o memorando de entendimento entre Portugal e a China sobre coopera&ccedil;&atilde;o no quadro da chamada &ldquo;nova rota da seda&rdquo;, com uma refer&ecirc;ncia &agrave; mobilidade el&eacute;ctrica. Deste memorando foi conhecido apenas um pequeno resumo, no qual se diz que se &ldquo;estabelece as modalidades de coopera&ccedil;&atilde;o bilateral no &acirc;mbito da iniciativa chinesa&nbsp;<em>one&nbsp;belt,&nbsp;one&nbsp;road</em>, abrangendo uma ampla gama de sectores, com destaque para a conectividade e para a mobilidade el&eacute;ctrica.', 'EDP e CTG reforçam relações culturais ... e mais algum texto para testar', 'Al&eacute;m dos acordos bilaterais, foram tamb&eacute;m assinados acordos empresariais entre as maiores empresas portuguesas e v&aacute;rios grupos chineses como a State Grid, a China Three Gorges (CTG), a Huawei, o Bank of China e a Union Pay. Rodrigo Costa (REN), Ant&oacute;nio Mexia (EDP), Alexandre Fonseca (Altice), Paulo Macedo (CGD), Gon&ccedil;alo Reis (RTP), Miguel Maya (BCP), foram alguns dos gestores presentes na sala dos espelhos do Pal&aacute;cio de Queluz para assinar acordos com as empresas chinesas.<br />\r\n<br />\r\nO presidente da EDP, Ant&oacute;nio Mexia, e o l&iacute;der da CTG, Lei Mingshan, assinaram um &quot;instrumento que define a coopera&ccedil;&atilde;o ao n&iacute;vel da responsabilidade social das empresas, designadamente no dom&iacute;nio da cultura (com enfoque em actividades a desenvolver pela Funda&ccedil;&atilde;o EDP e MAAT), desenvolvimento sustent&aacute;vel, inova&ccedil;&atilde;o e R&amp;D&quot;.&nbsp;', NULL, 3, 0, 0, NULL, NULL, 3, 'www.google.pt', '_blank', 'Ler notícia', 'www.google.pt', '_blank', 'Ler notícia', 'www.google.pt', '_blank', 'Ler notícia', NULL, 1, 17),
(41, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Teste 6 - 2 colunas', '', 'Assinatura dos acordos', 'Na Sala de Espelhos do Pal&aacute;cio Nacional de Queluz, e debaixo do olhar do Presidente da China, Xi Jinping, e do primeiro-ministro, Ant&oacute;nio Costa, foram rubricados 17 acordos envolvendo os dois Estados e empresas dos dois pa&iacute;ses.<br />\r\n<br />\r\nAqui, destaca-se o memorando de entendimento entre Portugal e China&nbsp;sobre coopera&ccedil;&atilde;o no quadro da chamada &ldquo;nova rota da seda&rdquo; (one belt, one road), com uma refer&ecirc;ncia &agrave; mobilidade el&eacute;ctrica, mas inclui-se tamb&eacute;m outro para a exporta&ccedil;&atilde;o de uva de mesa e a cria&ccedil;&atilde;o de um projecto empresarial chin&ecirc;s em Matosinhos que dever&aacute; criar 150 empregos.', 'Nova rota da seda', 'Ao todo, foram rubricados 17 acordos (e n&atilde;o 19 como chegou a ser avan&ccedil;ado), divididos entre Estado e empresas. Em primeiro lugar, surge o memorando de entendimento entre Portugal e a China sobre coopera&ccedil;&atilde;o no quadro da chamada &ldquo;nova rota da seda&rdquo;, com uma refer&ecirc;ncia &agrave; mobilidade el&eacute;ctrica. Deste memorando foi conhecido apenas um pequeno resumo, no qual se diz que se &ldquo;estabelece as modalidades de coopera&ccedil;&atilde;o bilateral no &acirc;mbito da iniciativa chinesa&nbsp;<em>one&nbsp;belt,&nbsp;one&nbsp;road</em>, abrangendo uma ampla gama de sectores, com destaque para a conectividade e para a mobilidade el&eacute;ctrica.', '', '', NULL, 3, 0, 0, NULL, NULL, 2, 'www.google.pt', '_blank', 'Ler notícia', 'www.google.pt', '_blank', 'Ler notícia', '', '', '', NULL, 1, 18),
(42, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Teste 7', 'Onde nos pode encontrar (teste de título com 2 linhas)', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id sagittis diam, nec ultricies est. Donec auctor, leo ac faucibus convallis, ligula orci rhoncus magna, non volutpat elit arcu in ante. Sed et neque pellentesque, rhoncus lacus sit amet, sagittis lacus. Ut commodo fringilla quam, in blandit justo fringilla vitae. Fusce accumsan, nulla quis auctor vehicula, dui lacus dictum orci, ut pulvinar erat nibh ac massa.<br />\r\n<br />\r\nMorbi sed ipsum gravida, laoreet lorem ac, elementum lacus. Donec varius sollicitudin efficitur. Vestibulum porttitor bibendum urna, sit amet finibus mauris condimentum et. Curabitur nec est ut nibh tincidunt porttitor rhoncus sit amet augue. Pellentesque bibendum risus eget tortor ultrices fermentum. Vestibulum ultrices libero non odio tincidunt cursus.<br />\r\n<br />\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id sagittis diam, nec ultricies est. Donec auctor, leo ac faucibus convallis, ligula orci rhoncus magna, non volutpat elit arcu in ante. Sed et neque pellentesque, rhoncus lacus sit amet, sagittis lacus. Ut commodo fringilla quam, in blandit justo fringilla vitae. Fusce accumsan, nulla quis auctor vehicula, dui lacus dictum orci, ut pulvinar erat nibh ac massa.<br />\r\n<br />\r\n<strong>Tamanho do mapa ao lado esquerdo acompanha a extens&atilde;o do texto</strong>', '', '', '', '', NULL, 4, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3081.039941841125!2d-9.017377384234168!3d39.4458292220341!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd18b07331ab4fad%3A0xab6eb3248629e927!2sIvo+Cutelarias%2C+Lda.!5e0!3m2!1spt-PT!2spt!4v1538577851739" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>', 1, 19),
(43, 1, 0, 1, NULL, NULL, 1, 0, NULL, 1, NULL, 1, NULL, 'Teste 8 - Upload de Ficheiros', 'Faça o download do manual!', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id sagittis diam, nec ultricies est. Donec auctor, leo ac faucibus convallis, ligula orci rhoncus magna, non volutpat elit arcu in ante. Sed et neque pellentesque, rhoncus lacus sit amet, sagittis lacus. Ut commodo fringilla quam, in blandit justo fringilla vitae.', '', '', '', '', NULL, 6, 0, 0, NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', NULL, 1, 20),
(44, 1, 0, 2, '', '052507_1_1977_imagem-de-teste.png', 1, 1, 0, 1, NULL, 1, NULL, 'Teste 9 - Timeline', 'Uma timeline com o título enorme, com 2 linhas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 1, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 21);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas_blocos_timeline_pt`
--

CREATE TABLE IF NOT EXISTS `paginas_blocos_timeline_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bloco` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem1` varchar(255) DEFAULT NULL,
  `ano` varchar(255) DEFAULT NULL,
  `visivel` tinyint(4) DEFAULT '1',
  `ordem` int(11) DEFAULT '99',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `paginas_blocos_timeline_pt`
--

INSERT INTO `paginas_blocos_timeline_pt` (`id`, `bloco`, `nome`, `titulo`, `texto`, `imagem1`, `ano`, `visivel`, `ordem`) VALUES
(1, 35, '2010', 'Ano 2010', 'Mauris dignissim massa ac nibh blandit, eu porttitor arcu feugiat. Proin in ante urna. Vestibulum fringilla eget eros in efficitur. Aliquam sem lectus, feugiat in egestas ut, mollis eu dui. Ut ut nibh interdum, molestie quam at, consectetur eros. Sed sapien ante, cursus cursus euismod blandit, molestie a ex. Donec fermentum purus enim, et posuere justo tempus eget. Morbi aliquet ultrices gravida. Sed id auctor metus. Nulla facilisi. Donec pretium dignissim ornare. Suspendisse cursus laoreet justo ac ornare. Donec tempus erat sed tellus tincidunt molestie.', '034136_1_3757_Destaque_05.jpg', '2010', 1, 99),
(2, 35, '2011', 'Título 2011', 'Proin aliquam massa et arcu congue, a aliquam nulla tincidunt. Aliquam viverra mi at ipsum semper eleifend. Morbi leo felis, ultricies sit amet rutrum et, egestas vel erat. Nunc dictum velit at ligula faucibus scelerisque vitae tempor nisi. Curabitur faucibus volutpat dolor, a semper est venenatis a. Cras placerat est eget lacus auctor, sed iaculis justo tincidunt. Aenean dapibus malesuada eros et vulputate. Pellentesque aliquam dui vitae justo hendrerit, sit amet dapibus elit dictum. Pellentesque imperdiet magna ac nisi rutrum pretium a vel mauris. Suspendisse id sapien at nisi tempus mattis non non lectus. Praesent pulvinar et ex id bibendum. Nulla facilisi. Fusce quis vehicula diam. Nulla suscipit quis ex ut sollicitudin. Pellentesque nisi neque, cursus vitae turpis non, finibus lacinia odio.', '040910_1_8401_Destaque_02.jpg', '2011', 1, 99),
(3, 35, '2013', 'Ano 2013', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum feugiat ex ac ex aliquam maximus. Aliquam sit amet mollis purus. Etiam at lectus et metus blandit consectetur. In sit amet semper augue. Vivamus id dui eget neque tincidunt rhoncus dictum sit amet ipsum. Phasellus tincidunt ante et quam vestibulum pharetra vitae ut purus. Nam pellentesque neque non convallis ornare. Nulla aliquam bibendum mattis. Fusce auctor nisl pulvinar, lacinia metus ac, pellentesque quam. Quisque et molestie nulla', '040950_1_5781_Destaque_01.jpg', '2013', 1, 99),
(4, 44, 'Time 1', 'Timeline 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id sagittis diam, nec ultricies est. Donec auctor, leo ac faucibus convallis, ligula orci rhoncus magna, non volutpat elit arcu in ante. Sed et neque pellentesque, rhoncus lacus sit amet, sagittis lacus. Ut commodo fringilla quam, in blandit justo fringilla vitae.<br />\r\n<br />\r\nFusce accumsan, nulla quis auctor vehicula, dui lacus dictum orci, ut pulvinar erat nibh ac massa. Morbi sed ipsum gravida, laoreet lorem ac, elementum lacus. Donec varius sollicitudin efficitur. Vestibulum porttitor bibendum urna, sit amet finibus mauris condimentum et. Curabitur nec est ut nibh tincidunt porttitor rhoncus sit amet augue. Pellentesque bibendum risus eget tortor ultrices fermentum. Vestibulum ultrices libero non odio tincidunt cursus.', '052533_1_6395_imagem-de-teste-2.png', '2010', 1, 99),
(5, 44, 'Time 2', 'Timeline 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id sagittis diam, nec ultricies est. Donec auctor, leo ac faucibus convallis, ligula orci rhoncus magna, non volutpat elit arcu in ante. Sed et neque pellentesque, rhoncus lacus sit amet, sagittis lacus.', '052541_1_4268_imagem-de-teste.png', '2020', 1, 99);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas_pt`
--

CREATE TABLE IF NOT EXISTS `paginas_pt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem1` varchar(255) DEFAULT NULL,
  `mostrar_menu` tinyint(4) DEFAULT '0',
  `mostrar_topo` tinyint(4) DEFAULT '1',
  `esp_blocos` int(11) DEFAULT '120',
  `esp_blocos_mob` int(11) DEFAULT '60',
  `mostra_titulo` tinyint(4) DEFAULT '1' COMMENT '1 - Mostra título no topo da página; 0 - não mostra',
  `cor_titulo` varchar(20) DEFAULT '#000000',
  `tem_fundo` tinyint(4) DEFAULT '1' COMMENT '1 - Possui cor ou imagem de fundo; 0 - não tem',
  `tipo_fundo` tinyint(4) DEFAULT '1' COMMENT '1 - Cor; 2 - Imagem',
  `cor_fundo` varchar(50) DEFAULT '#e2e2e2',
  `fixo` tinyint(4) DEFAULT '0',
  `ordem` int(11) DEFAULT '99',
  `visivel` tinyint(4) DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  PRIMARY KEY (`id`),
  KEY `indice` (`mostrar_topo`,`mostra_titulo`,`tem_fundo`,`tipo_fundo`,`fixo`,`ordem`,`visivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `paginas_pt`
--

INSERT INTO `paginas_pt` (`id`, `menu`, `nome`, `titulo`, `imagem1`, `mostrar_menu`, `mostrar_topo`, `esp_blocos`, `esp_blocos_mob`, `mostra_titulo`, `cor_titulo`, `tem_fundo`, `tipo_fundo`, `cor_fundo`, `fixo`, `ordem`, `visivel`, `url`, `title`, `description`, `keywords`) VALUES
(1, NULL, 'Teste', 'Isto é um teste de um título, para tentar simular uma quebra de linha', '085432_1_1847_topo_paginas.jpg', 0, 1, 120, 60, 1, '#FFFFFF', 1, 2, '#E2E2E2', 0, 99, 1, 'teste', 'Teste', NULL, NULL),
(2, NULL, 'Política de Cookies', 'Política de Cookies', NULL, 0, 1, 120, 60, 1, '#000000', 1, 1, '#e2e2e2', 1, 99, 1, 'politica-cookies', 'Política de Cookies', NULL, NULL),
(3, NULL, 'Política de Privacidade', 'Política de Privacidade', NULL, 0, 1, 120, 60, 1, '#000000', 1, 1, '#e2e2e2', 1, 99, 1, 'politica-de-privacidade', 'Política de Privacidade', NULL, NULL),
(4, NULL, 'RAL e RLL', 'RAL e RLL', NULL, 0, 1, 120, 60, 1, '#000000', 1, 1, '#e2e2e2', 1, 99, 1, 'ral-e-rll', 'RAL e RLL', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paises`
--

CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `abreviatura` varchar(20) DEFAULT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `zona` int(11) DEFAULT '1',
  `idioma` int(11) DEFAULT '1',
  `moeda` int(11) DEFAULT '1',
  `visivel` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indice` (`abreviatura`,`codigo`,`zona`,`visivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=270 ;

--
-- Extraindo dados da tabela `paises`
--

INSERT INTO `paises` (`id`, `nome`, `abreviatura`, `codigo`, `zona`, `idioma`, `moeda`, `visivel`) VALUES
(2, 'Belgium', 'BEL', 'BE', 1, 1, 1, 1),
(11, 'Afghanistan', 'AFG', 'AF', 1, 1, 1, 0),
(12, 'Akrotiri', NULL, NULL, 1, 1, 1, 0),
(13, 'Albania', 'ALB', 'AL', 1, 1, 1, 0),
(14, 'Algeria', 'DZA', 'DZ', 1, 1, 1, 0),
(15, 'American Samoa', 'ASM', 'AS', 1, 1, 1, 0),
(16, 'Andorra', 'AND', 'AD', 1, 1, 1, 1),
(17, 'Angola', 'AGO', 'AO', 1, 1, 1, 0),
(18, 'Anguilla', 'AIA', 'AI', 1, 1, 1, 0),
(19, 'Antarctica', NULL, 'AQ', 1, 1, 1, 0),
(20, 'Antigua and Barbuda', 'ATG', 'AG', 1, 1, 1, 0),
(21, 'Argentina', 'ARG', 'AR', 1, 1, 1, 0),
(22, 'Armenia', 'ARM', 'AM', 1, 1, 1, 1),
(23, 'Aruba', 'ABW', 'AW', 1, 1, 1, 0),
(24, 'Ashmore and Cartier Islands', NULL, NULL, 1, 1, 1, 0),
(25, 'Australia', 'AUS', 'AU', 1, 1, 1, 0),
(26, 'Austria', 'AUT', 'AT', 1, 1, 1, 1),
(27, 'Azerbaijan', 'AZE', 'AZ', 1, 1, 1, 0),
(28, 'Bahamas, The', 'BHS', 'BS', 1, 1, 1, 0),
(29, 'Bahrain', 'BHR', 'BH', 1, 1, 1, 0),
(30, 'Bangladesh', 'BGD', 'BD', 1, 1, 1, 0),
(31, 'Barbados', 'BRB', 'BB', 1, 1, 1, 0),
(32, 'Bassas da India', NULL, NULL, 1, 1, 1, 0),
(33, 'Belarus', 'BLR', 'BY', 1, 1, 1, 0),
(34, 'Bolivia', 'BOL', 'BO', 1, 1, 1, 0),
(35, 'Belize', 'BLZ', 'BZ', 1, 1, 1, 0),
(36, 'Benin', 'BEN', 'BJ', 1, 1, 1, 0),
(37, 'Bermuda', 'BMU', 'BM', 1, 1, 1, 0),
(38, 'Bhutan', 'BTN', 'BT', 1, 1, 1, 0),
(39, 'Bosnia and Herzegovina', 'BIH', 'BA', 1, 1, 1, 0),
(40, 'Botswana', 'BWA', 'BW', 1, 1, 1, 0),
(41, 'Bouvet Island', NULL, 'BV', 1, 1, 1, 0),
(42, 'Brazil', 'BRA', 'BR', 1, 1, 1, 0),
(43, 'British Indian Ocean Territory', NULL, 'IO', 1, 1, 1, 0),
(44, 'British Virgin Islands', NULL, NULL, 1, 1, 1, 0),
(45, 'Brunei', 'BRN', 'BN', 1, 1, 1, 0),
(46, 'Bulgaria', 'BGR', 'BG', 1, 1, 1, 1),
(47, 'Burkina Faso', 'BFA', 'BF', 1, 1, 1, 0),
(48, 'Burma', NULL, NULL, 1, 1, 1, 0),
(49, 'Burundi', 'BDI', 'BI', 1, 1, 1, 0),
(50, 'Cambodia', 'KHM', 'KH', 1, 1, 1, 0),
(51, 'Cameroon', 'CMR', 'CM', 1, 1, 1, 0),
(52, 'Canada', 'CAN', 'CA', 1, 1, 1, 0),
(53, 'Cape Verde', 'CPV', 'CV', 1, 1, 1, 0),
(54, 'Cayman Islands', 'CYM', 'KY', 1, 1, 1, 0),
(55, 'Central African Republic', 'CAF', 'CF', 1, 1, 1, 0),
(56, 'Chad', 'TCD', 'TD', 1, 1, 1, 0),
(57, 'Chile', 'CHL', 'CL', 1, 1, 1, 0),
(58, 'China', 'CHN', 'CN', 1, 1, 1, 0),
(59, 'Christmas Island', NULL, 'CX', 1, 1, 1, 0),
(60, 'Clipperton Island', NULL, NULL, 1, 1, 1, 0),
(61, 'Cocos (Keeling) Islands', NULL, 'CC', 1, 1, 1, 0),
(62, 'Colombia', 'COL', 'CO', 1, 1, 1, 0),
(63, 'Comoros', 'COM', 'KM', 1, 1, 1, 0),
(64, 'Congo, Democratic Republic of the', 'COD', 'CD', 1, 1, 1, 0),
(65, 'Congo, Republic of the', 'COG', 'CG', 1, 1, 1, 0),
(66, 'Cook Islands', 'COK', 'CK', 1, 1, 1, 0),
(67, 'Coral Sea Islands', NULL, NULL, 1, 1, 1, 0),
(68, 'Costa Rica', 'CRI', 'CR', 1, 1, 1, 0),
(69, 'Cote d''Ivoire', 'CIV', 'CI', 1, 1, 1, 0),
(70, 'Croatia', 'HRV', 'HR', 1, 1, 1, 1),
(71, 'Cuba', 'CUB', 'CU', 1, 1, 1, 0),
(72, 'Cyprus', 'CYP', 'CY', 1, 1, 1, 0),
(73, 'Czech Republic', 'CZE', 'CZ', 1, 1, 1, 0),
(74, 'Denmark', 'DNK', 'DK', 1, 1, 1, 1),
(75, 'Dhekelia', NULL, NULL, 1, 1, 1, 0),
(76, 'Djibouti', 'DJI', 'DJ', 1, 1, 1, 0),
(77, 'Dominica', 'DMA', 'DM', 1, 1, 1, 0),
(78, 'Dominican Republic', 'DOM', 'DO', 1, 1, 1, 0),
(79, 'Ecuador', 'ECU', 'EC', 1, 1, 1, 0),
(80, 'Egypt', 'EGY', 'EG', 1, 1, 1, 0),
(81, 'El Salvador', 'SLV', 'SV', 1, 1, 1, 0),
(82, 'Equatorial Guinea', 'GNQ', 'GQ', 1, 1, 1, 0),
(83, 'Eritrea', 'ERI', 'ER', 1, 1, 1, 0),
(84, 'Estonia', 'EST', 'EE', 1, 1, 1, 0),
(85, 'Ethiopia', 'ETH', 'ET', 1, 1, 1, 0),
(86, 'Europa Island', NULL, NULL, 1, 1, 1, 0),
(87, 'Falkland Islands (Islas Malvinas)', NULL, NULL, 1, 1, 1, 0),
(88, 'Faroe Islands', 'FRO', 'FO', 1, 1, 1, 0),
(89, 'Fiji\r\n', 'FJI', 'FJ', 1, 1, 1, 0),
(90, 'Finland', 'FIN', 'FI', 1, 1, 1, 1),
(91, 'France', 'FRA', 'FR', 1, 1, 1, 1),
(92, 'French Guiana', 'GUF', 'GF', 1, 1, 1, 1),
(93, 'French Polynesia', 'PYF', 'PF', 1, 1, 1, 1),
(94, 'French Southern and Antarctic Lands', NULL, 'TF', 1, 1, 1, 1),
(95, 'Gabon', 'GAB', 'GA', 1, 1, 1, 0),
(96, 'Gambia, The', 'GMB', 'GM', 1, 1, 1, 0),
(97, 'Gaza Strip', NULL, NULL, 1, 1, 1, 0),
(98, 'Georgia', 'GEO', 'GE', 1, 1, 1, 1),
(99, 'Germany', 'DEU', 'DE', 1, 1, 1, 1),
(100, 'Ghana', 'GHA', 'GH', 1, 1, 1, 0),
(101, 'Gibraltar', 'GIB', 'GI', 1, 1, 1, 0),
(102, 'Glorioso Islands', NULL, NULL, 1, 1, 1, 0),
(103, 'Greece', 'GRC', 'GR', 1, 1, 1, 1),
(105, 'Grenada', 'GRD', 'GD', 1, 1, 1, 0),
(106, 'Guadeloupe', 'GLP', 'GP', 1, 1, 1, 0),
(107, 'Guam', 'GUM', 'GU', 1, 1, 1, 0),
(108, 'Guatemala', 'GTM', 'GT', 1, 1, 1, 0),
(109, 'Guernsey', NULL, NULL, 1, 1, 1, 0),
(110, 'Guinea', 'GIN', 'GN', 1, 1, 1, 0),
(111, 'Guinea-Bissau', 'GNB', 'GW', 1, 1, 1, 0),
(112, 'Guyana', 'GUY', 'GY', 1, 1, 1, 0),
(113, 'Haiti', 'HTI', 'HT', 1, 1, 1, 0),
(114, 'Heard Island and Mcdonald Islands', NULL, 'HM', 1, 1, 1, 0),
(115, 'Holy See (Vatican City State)', 'VAT', 'VA', 1, 1, 1, 0),
(116, 'Honduras', 'HND', 'HN', 1, 1, 1, 0),
(117, 'Hong Kong', 'HKG', 'HK', 1, 1, 1, 0),
(118, 'Hungary', 'HUN', 'HU', 1, 1, 1, 1),
(119, 'Iceland', 'ISL', 'IS', 1, 1, 1, 0),
(120, 'India', 'IND', 'IN', 1, 1, 1, 0),
(121, 'Indonesia', 'IDN', 'ID', 1, 1, 1, 0),
(122, 'Iran, Islamic Republic of', 'IRN', 'IR', 1, 1, 1, 0),
(123, 'Iraq', 'IRQ', 'IQ', 1, 1, 1, 0),
(124, 'Ireland', 'IRL', 'IE', 1, 1, 1, 1),
(125, 'Isle of Man', NULL, NULL, 1, 1, 1, 0),
(126, 'Israel', 'ISR', 'IL', 1, 1, 1, 0),
(127, 'Italy', 'ITA', 'IT', 1, 1, 1, 1),
(128, 'Jamaica', 'JAM', 'JM', 1, 1, 1, 0),
(129, 'Jan Mayen', NULL, NULL, 1, 1, 1, 0),
(130, 'Japan', 'JPN', 'JP', 1, 1, 1, 0),
(131, 'Jersey', NULL, NULL, 1, 1, 1, 0),
(132, 'Jordan', 'JOR', 'JO', 1, 1, 1, 0),
(133, 'Juan de Nova Island', NULL, NULL, 1, 1, 1, 0),
(134, 'Kazakhstan', 'KAZ', 'KZ', 1, 1, 1, 0),
(135, 'Kenya', 'KEN', 'KE', 1, 1, 1, 0),
(136, 'Kiribati', 'KIR', 'KI', 1, 1, 1, 0),
(137, 'Korea, North', 'PRK', 'KP', 1, 1, 1, 0),
(138, 'Korea, South', 'KOR', 'KR', 1, 1, 1, 0),
(139, 'Kuwait', 'KWT', 'KW', 1, 1, 1, 0),
(140, 'Kyrgyzstan', 'KGZ', 'KG', 1, 1, 1, 0),
(141, 'Laos', 'LAO', 'LA', 1, 1, 1, 0),
(142, 'Latvia', 'LVA', 'LV', 1, 1, 1, 0),
(143, 'Lebanon', 'LBN', 'LB', 1, 1, 1, 0),
(144, 'Lesotho', 'LSO', 'LS', 1, 1, 1, 0),
(145, 'Liberia', 'LBR', 'LR', 1, 1, 1, 0),
(146, 'Libya', 'LBY', 'LY', 1, 1, 1, 0),
(147, 'Liechtenstein', 'LIE', 'LI', 1, 1, 1, 0),
(148, 'Lithuania', 'LTU', 'LT', 1, 1, 1, 0),
(149, 'Luxembourg', 'LUX', 'LU', 1, 1, 1, 1),
(150, 'Macao', 'MAC', 'MO', 1, 1, 1, 0),
(151, 'Macedonia', 'MKD', 'MK', 1, 1, 1, 0),
(152, 'Madagascar', 'MDG', 'MG', 1, 1, 1, 0),
(153, 'Malawi', 'MWI', 'MW', 1, 1, 1, 0),
(154, 'Malaysia', 'MYS', 'MY', 1, 1, 1, 0),
(155, 'Maldives', 'MDV', 'MV', 1, 1, 1, 0),
(156, 'Mali', 'MLI', 'ML', 1, 1, 1, 0),
(157, 'Malta', 'MLT', 'MT', 1, 1, 1, 0),
(158, 'Marshall Islands', 'MHL', 'MH', 1, 1, 1, 0),
(159, 'Martinique', 'MTQ', 'MQ', 1, 1, 1, 0),
(160, 'Mauritania', 'MRT', 'MR', 1, 1, 1, 0),
(161, 'Mauritius', 'MUS', 'MU', 1, 1, 1, 0),
(162, 'Mayotte', NULL, 'YT', 1, 1, 1, 0),
(163, 'Mexico', 'MEX', 'MX', 1, 1, 1, 0),
(164, 'Micronesia, Federated States of', 'FSM', 'FM', 1, 1, 1, 0),
(165, 'Moldova', 'MDA', 'MD', 1, 1, 1, 0),
(166, 'Monaco', 'MCO', 'MC', 1, 1, 1, 1),
(167, 'Mongolia', 'MNG', 'MN', 1, 1, 1, 0),
(168, 'Montserrat', 'MSR', 'MS', 1, 1, 1, 0),
(169, 'Morocco', 'MAR', 'MA', 1, 1, 1, 0),
(170, 'Mozambique', 'MOZ', 'MZ', 1, 1, 1, 0),
(171, 'Namibia', 'NAM', 'NA', 1, 1, 1, 0),
(172, 'Nauru', 'NRU', 'NR', 1, 1, 1, 0),
(173, 'Navassa Island', NULL, NULL, 1, 1, 1, 0),
(174, 'Nepal', 'NPL', 'NP', 1, 1, 1, 0),
(175, 'Netherlands', 'NLD', 'NL', 1, 1, 1, 1),
(176, 'Netherlands Antilles', 'ANT', 'AN', 1, 1, 1, 1),
(177, 'New Caledonia', 'NCL', 'NC', 1, 1, 1, 0),
(178, 'New Zealand', 'NZL', 'NZ', 1, 1, 1, 0),
(179, 'Nicaragua', 'NIC', 'NI', 1, 1, 1, 0),
(180, 'Niger', 'NER', 'NE', 1, 1, 1, 0),
(181, 'Nigeria', 'NGA', 'NG', 1, 1, 1, 0),
(182, 'Niue', 'NIU', 'NU', 1, 1, 1, 0),
(183, 'Norfolk Island', 'NFK', 'NF', 1, 1, 1, 0),
(184, 'Northern Mariana Islands', 'MNP', 'MP', 1, 1, 1, 0),
(185, 'Norway', 'NOR', 'NO', 1, 1, 1, 1),
(186, 'Oman', 'OMN', 'OM', 1, 1, 1, 0),
(187, 'Pakistan', 'PAK', 'PK', 1, 1, 1, 0),
(188, 'Palau', 'PLW', 'PW', 1, 1, 1, 0),
(189, 'Panama', 'PAN', 'PA', 1, 1, 1, 0),
(190, 'Papua New Guinea', 'PNG', 'PG', 1, 1, 1, 0),
(191, 'Paracel Islands', NULL, NULL, 1, 1, 1, 0),
(192, 'Paraguay', 'PRY', 'PY', 1, 1, 1, 0),
(193, 'Peru', 'PER', 'PE', 1, 1, 1, 0),
(194, 'Philippines', 'PHL', 'PH', 1, 1, 1, 0),
(195, 'Pitcairn Islands', 'PCN', 'PN', 1, 1, 1, 0),
(196, 'Poland', 'POL', 'PL', 1, 1, 1, 1),
(197, 'Portugal Continental', 'PRT', 'PT', 2, 1, 1, 1),
(198, 'Puerto Rico', 'PRI', 'PR', 1, 1, 1, 0),
(199, 'Qatar', 'QAT', 'QA', 1, 1, 1, 0),
(200, 'Reunion', 'REU', 'RE', 1, 1, 1, 0),
(201, 'Romania', 'ROM', 'RO', 1, 1, 1, 0),
(202, 'Russia', 'RUS', 'RU', 1, 1, 1, 1),
(203, 'Rwanda', 'RWA', 'RW', 1, 1, 1, 0),
(204, 'Saint Helena', 'SHN', 'SH', 1, 1, 1, 0),
(205, 'Saint Kitts and Nevis', 'KNA', 'KN', 1, 1, 1, 0),
(206, 'Saint Lucia', 'LCA', 'LC', 1, 1, 1, 0),
(207, 'Saint Pierre and Miquelon', 'SPM', 'PM', 1, 1, 1, 0),
(208, 'Saint Vincent and the Grenadines', 'VCT', 'VC', 1, 1, 1, 0),
(209, 'Samoa', 'WSM', 'WS', 1, 1, 1, 0),
(210, 'San Marino', 'SMR', 'SM', 1, 1, 1, 0),
(211, 'Sao Tome and Principe', 'STP', 'ST', 1, 1, 1, 0),
(212, 'Saudi Arabia', 'SAU', 'SA', 1, 1, 1, 0),
(213, 'Senegal', 'SEN', 'SN', 1, 1, 1, 0),
(214, 'Serbia and Montenegro', NULL, 'RS', 1, 1, 1, 1),
(215, 'Seychelles', 'SYC', 'SC', 1, 1, 1, 0),
(216, 'Sierra Leone', 'SLE', 'SL', 1, 1, 1, 0),
(217, 'Singapore', 'SGP', 'SG', 1, 1, 1, 0),
(218, 'Slovakia', 'SVK', 'SK', 1, 1, 1, 1),
(219, 'Slovenia', 'SVN', 'SI', 1, 1, 1, 1),
(220, 'Solomon Islands', 'SLB', 'SB', 1, 1, 1, 0),
(221, 'Somalia', 'SOM', 'SO', 1, 1, 1, 0),
(222, 'South Africa', 'ZAF', 'ZA', 1, 1, 1, 0),
(223, 'South Georgia and the South Sandwich Islands', NULL, 'GS', 1, 1, 1, 0),
(224, 'España Peninsular', 'ESP', 'ES', 1, 1, 1, 1),
(225, 'Spratly Islands', NULL, NULL, 1, 1, 1, 0),
(226, 'Sri Lanka', 'LKA', 'LK', 1, 1, 1, 0),
(227, 'Sudan', 'SDN', 'SD', 1, 1, 1, 0),
(228, 'Suriname', 'SUR', 'SR', 1, 1, 1, 0),
(229, 'Svalbard', NULL, NULL, 1, 1, 1, 0),
(230, 'Swaziland', 'SWZ', 'SZ', 1, 1, 1, 0),
(231, 'Sweden', 'SWE', 'SE', 1, 1, 1, 1),
(232, 'Switzerland', 'CHE', 'CH', 1, 1, 1, 1),
(233, 'Syria', 'SYR', 'SY', 1, 1, 1, 0),
(234, 'Taiwan', 'TWN', 'TW', 1, 1, 1, 0),
(235, 'Tajikistan', 'TJK', 'TJ', 1, 1, 1, 0),
(236, 'Tanzania', 'TZA', 'TZ', 1, 1, 1, 0),
(237, 'Thailand', 'THA', 'TH', 1, 1, 1, 0),
(238, 'Timor-Leste', NULL, 'TL', 1, 1, 1, 0),
(239, 'Togo', 'TGO', 'TG', 1, 1, 1, 0),
(240, 'Tokelau', 'TKL', 'TK', 1, 1, 1, 0),
(241, 'Tonga', 'TON', 'TO', 1, 1, 1, 0),
(242, 'Trinidad and Tobago', 'TTO', 'TT', 1, 1, 1, 0),
(243, 'Tromelin Island', NULL, NULL, 1, 1, 1, 0),
(244, 'Tunisia', 'TUN', 'TN', 1, 1, 1, 1),
(245, 'Turkey', 'TUR', 'TR', 1, 1, 1, 1),
(246, 'Turkmenistan', 'TKM', 'TM', 1, 1, 1, 0),
(247, 'Turks and Caicos Islands', 'TCA', 'TC', 1, 1, 1, 0),
(248, 'Tuvalu', 'TUV', 'TV', 1, 1, 1, 0),
(249, 'Uganda', 'UGA', 'UG', 1, 1, 1, 0),
(250, 'Ukraine', 'UKR', 'UA', 1, 1, 1, 1),
(251, 'United Arab Emirates', 'ARE', 'AE', 1, 1, 1, 0),
(252, 'United Kingdom', 'GBR', 'GB', 1, 1, 1, 1),
(253, 'United States', 'USA', 'US', 1, 1, 1, 0),
(254, 'Uruguay', 'URY', 'UY', 1, 1, 1, 0),
(255, 'Uzbekistan', 'UZB', 'UZ', 1, 1, 1, 0),
(256, 'Vanuatu', 'VUT', 'VU', 1, 1, 1, 0),
(257, 'Venezuela', 'VEN', 'VE', 1, 1, 1, 0),
(258, 'Vietnam', 'VNM', 'VN', 1, 1, 1, 0),
(259, 'Virgin Islands, British', 'VGB', 'VG', 1, 1, 1, 0),
(260, 'Wake Island', NULL, NULL, 1, 1, 1, 0),
(261, 'Wallis and Futuna', 'WLF', 'WF', 1, 1, 1, 0),
(262, 'West Bank', NULL, NULL, 1, 1, 1, 0),
(263, 'Western Sahara', 'ESH', 'EH', 1, 1, 1, 0),
(264, 'Yemen', 'YEM', 'YE', 1, 1, 1, 0),
(265, 'Zambia', 'ZMB', 'ZM', 1, 1, 1, 0),
(266, 'Zimbabwe', 'ZWE', 'ZW', 1, 1, 1, 0),
(267, 'Portugal Ilhas', 'PRT', 'PT', 3, 1, 1, 1),
(268, 'España Baleares', 'ESP', 'ES', 1, 1, 1, 0),
(269, 'España Canarias', 'ESP', 'ES', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `redes_sociais`
--

CREATE TABLE IF NOT EXISTS `redes_sociais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `link` text,
  `mostra_topo` tinyint(4) DEFAULT '0',
  `visivel` tinyint(4) DEFAULT '1',
  `ordem` int(11) DEFAULT '99',
  PRIMARY KEY (`id`),
  KEY `indice` (`visivel`,`ordem`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `redes_sociais`
--

INSERT INTO `redes_sociais` (`id`, `nome`, `link`, `mostra_topo`, `visivel`, `ordem`) VALUES
(1, 'Facebook', '', 1, 1, 10),
(2, 'Vimeo', '', 1, 1, 40),
(3, 'Twitter', '', 1, 1, 60),
(4, 'Pinterest', '', 1, 1, 50),
(5, 'Instagram', '', 1, 1, 20),
(6, 'Google', '', 0, 1, 30),
(7, 'Blogger', '', 0, 1, 70),
(8, 'Linkedin', '', 0, 1, 80),
(9, 'Youtube', '', 0, 1, 90);

-- --------------------------------------------------------

--
-- Estrutura da tabela `redirects_301`
--

CREATE TABLE IF NOT EXISTS `redirects_301` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_old` varchar(500) NOT NULL,
  `url_new` varchar(500) NOT NULL,
  `lang` varchar(20) DEFAULT 'pt',
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `share_logs`
--

CREATE TABLE IF NOT EXISTS `share_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_envio` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `facebook` int(11) DEFAULT NULL,
  `twitter` int(11) DEFAULT NULL,
  `id_prod` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sitemap`
--

CREATE TABLE IF NOT EXISTS `sitemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `links` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sitemap_estatico`
--

CREATE TABLE IF NOT EXISTS `sitemap_estatico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text,
  `prioridade` decimal(5,1) DEFAULT '1.0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sitemap_tabelas`
--

CREATE TABLE IF NOT EXISTS `sitemap_tabelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  `prefixo_url` varchar(250) DEFAULT NULL,
  `linguas` tinyint(4) DEFAULT '1',
  `imagem` varchar(250) DEFAULT NULL,
  `pasta` varchar(250) DEFAULT NULL,
  `prioridade` decimal(5,1) DEFAULT NULL,
  `blog` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `termos_pesquisa`
--

CREATE TABLE IF NOT EXISTS `termos_pesquisa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termo` varchar(255) DEFAULT NULL,
  `counter` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `indice` (`counter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
