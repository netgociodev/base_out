<?php
ob_start();
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED & ~E_STRICT);

# 0 localhost
# 1 suez
# 2 proposta
# 3 produ��o
define("ENV", 0);

//FRONTEND
if(!strstr($_SERVER["PHP_SELF"], "consola/")) {
	//ini_set("display_errors", "1");
}

//CONSOLA
if(strstr($_SERVER["PHP_SELF"], "consola/")) {
	//ini_set("display_errors", "1");
}



//Usado no Google Analytics (codigo_antes_body.php)
define("ANALYTICS", "UA-99999999-9");
define("SERVIDOR", "teste.pt");
define("SERVIDOR_ARRAY", serialize(array("teste.pt", "www.teste.pt")));
$array_servidor = unserialize(SERVIDOR_ARRAY);
define("NOME_SITE", "clinicacentraldaareosa");
define("COR_SITE", "#285975");
define("NTG_PROPO", 0);

// BASE DE DADOS
define("DB_HOSTNAME", "localhost");
if(!in_array($_SERVER['HTTP_HOST'], $array_servidor) && NTG_PROPO==0) { 
	####LOCAL_BD####
	define("DB_USERNAME", "netgocio");
	define("DB_PASSWORD", 'JH78ekhdk');
	define("DB_DATABASE", "clinicacentraldaareosa_2020");
	define("CAPTCHA_KEY", '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI');
	define("CAPTCHA_SECRET", '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe');
	define("MAPS_KEY", 'AIzaSyCnjcj-bGomkybPutgzFPg4uvAZ3yHOI9w');
}
else { 
	####ONLINE_BD####
	define("DB_USERNAME", "netgocio");
	define("DB_PASSWORD", 'JH78ekhdk');
	define("DB_DATABASE", "clinicacentraldaareosa_2020");
	define("CAPTCHA_KEY", '6LdEfDUUAAAAAK9LI0nvRR1PNZE2X9b_r4A9pozw');
	define("CAPTCHA_SECRET", '6LdEfDUUAAAAAMCy232O5ZoXOMCelvdIieUqh-Tm');
	define("MAPS_KEY", '');
}

//VARIAVEIS E-COMMERCE
define("PESQ_TYPE", 0); //0=nenhum; 1=Modal; 2=autocomplete;
define("ECOMMERCE", 0);
define("ECC_MARCAS", 0);

if(!in_array($_SERVER['HTTP_HOST'], $array_servidor) && NTG_PROPO==0) { 
	####LOCAL_PATH####
	if (ENV == 0) {
		define("HTTP_DIR", "http://localhost"); //Usado nas Newsletters e Encomendas (encomendas-edit.php, newsletter-edit.php e newsletter-processar-envio.php)
		define("ROOTPATH", dirname(__DIR__)."/");
		define("ROOTPATH_HTTP", "http://".$_SERVER["HTTP_HOST"]."/");
	}
	else{
		define("HTTP_DIR", "http://192.168.1.230/trabalhos/C/clinicacentraldaareosa/site"); //Usado nas Newsletters e Encomendas (encomendas-edit.php, newsletter-edit.php e newsletter-processar-envio.php)
		define("ROOTPATH", "/media/trabalhos/C/clinicacentraldaareosa/site/");
		define("ROOTPATH_HTTP", "http://".$_SERVER["HTTP_HOST"]."/trabalhos/C/clinicacentraldaareosa/site/");
	}
}
else { 
	####ONLINE_BD####
	define("HTTP_DIR", "http://192.168.1.230/trabalhos/C/clinicacentraldaareosa/site"); //Usado nas Newsletters e Encomendas (encomendas-edit.php, newsletter-edit.php e newsletter-processar-envio.php)
	define("ROOTPATH", "/media/trabalhos/C/clinicacentraldaareosa/site/");
	define("ROOTPATH_HTTP", "http://".$_SERVER["HTTP_HOST"]."/trabalhos/C/clinicacentraldaareosa/site/");
}

define("ROOTPATH_CONSOLA", ROOTPATH."consola/"); 
define("ROOTPATH_ADMIN", ROOTPATH."consola/admin/"); 

define("ROOTPATH_HTTP_BLOG", ROOTPATH_HTTP."blog/");
define("ROOTPATH_HTTP_CONSOLA", ROOTPATH_HTTP."consola/"); 
define("ROOTPATH_HTTP_ADMIN", ROOTPATH_HTTP."consola/admin/"); 

// define("FONT_SITE", "custom: {
// 	families: ['Metropolis:n4,n6,n7', 'Unna:i4,i7'],
// 	urls: ['".ROOTPATH_HTTP."css/fonts.css']
// }");

define("FONT_SITE", "google: {families: ['Poppins:300,400,500,600,700']}");

class db {

	/*** Declare instance ***/
	private static $instance = NULL;
	
	/**
	*
	* the constructor is set to private so
	* so nobody can create a new instance using new
	*
	*/
	private function __construct() {
	  /*** maybe set the db name here later ***/
	}
	
	/**
	*
	* Return DB instance or create intitial connection
	*
	* @return object (PDO)
	*
	* @access public
	*
	*/
	public static function getInstance() {
		if (!self::$instance) {
			self::$instance = new PDO("mysql:host=".DB_HOSTNAME.";dbname=".DB_DATABASE, DB_USERNAME, DB_PASSWORD);
			//self::$instance-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
			self::$instance-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		
		return self::$instance;
	}
	
	public static function close() {
		if (self::$instance) {
			self::$instance = null;
		}
	}
	
	/**
	*
	* Like the constructor, we make __clone private
	* so nobody can clone the instance
	*
	*/
	private function __clone(){
	}

} /*** end of class ***/

$query_rsManutencao = "SELECT * FROM config WHERE id=1";
$rsManutencao = DB::getInstance()->prepare($query_rsManutencao);
$rsManutencao->execute();
$row_rsManutencao = $rsManutencao->fetch(PDO::FETCH_ASSOC);
$totalRows_rsManutencao = $rsManutencao->rowCount();
DB::close();

$ips = explode(",", str_replace(" ", "", $row_rsManutencao["ips"]));

$ip=$_SERVER["REMOTE_ADDR"];

if($ip==""){
	$ip=$HTTP_SERVER_VARS["REMOTE_ADDR"];
}

if($row_rsManutencao["manutencao"] == 1 && (!in_array($ip, $ips)) && $is_cron_file!=1){
	if(!strstr($_SERVER["PHP_SELF"], "manutencao.php") && !strstr($_SERVER["PHP_SELF"], "consola/")){
		header("Location: ".ROOTPATH_HTTP."manutencao.php");
		exit();
	}
}

if (!isset($_SESSION)) {
  session_start();
}

/* VARI�VEIS POR DEFEITO PARA CRONS */
if($is_cron_file == 1){
	//lingua
	$lang = "pt";
	$_SESSION["LANG"] = 'pt';

	//moeda
	$_COOKIE['SITE_currency'] = "EUR-&euro;";
}

/*HELPERS*/
include_once(ROOTPATH.'sendMail/send_mail.php');
include_once(ROOTPATH.'helpers/funcoes_base.php');
// Vari�vel para usar ao criar o cookie, se o servidor tiver certificado SSL instalado torna o cookie seguro
$cookie_secure = false;
if(function_exists('isSecure') && isSecure()) {
	$cookie_secure = true;
}

// Se n�o vier do redirect / caso venha tem l� o include, depois de incluir o l�nguas
if(!strstr($_SERVER['REQUEST_URI'],"/consola/")) {
	include_once(ROOTPATH.'MobileDetect.php');
	$detect = new Mobile_Detect;

	if(!$redirect) {
		require_once(ROOTPATH.'linguasLG.php'); 
		$extensao=$Recursos->Resources["extensao"];
		
		include_once(ROOTPATH.'helpers/sessions.php');
		include_once(ROOTPATH.'helpers/handle-forms.php');
	}
}
?>