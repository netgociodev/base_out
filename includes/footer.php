<?php if($footer_small == 0) { 
  if(tableExists(DB::getInstance(), 'met_pagamento_pt')) {
    $query_rsPagamento = "SELECT nome, imagem2 FROM met_pagamento".$extensao." WHERE visivel = 1 AND visivel_footer = 1 AND imagem2 != '' ORDER BY ordem ASC";
    $rsPagamento = DB::getInstance()->prepare($query_rsPagamento);
    $rsPagamento->execute();    
    $row_rsPagamento = $rsPagamento->fetchAll(PDO::FETCH_ASSOC);
    $totalRows_rsPagamento = $rsPagamento->rowCount();
  }
  if(tableExists(DB::getInstance(), 'met_envio_pt')){
    $query_rsEnvio = "SELECT nome, imagem2 FROM met_envio".$extensao." WHERE visivel_footer = 1 AND imagem2 != '' ORDER BY ordem ASC";
    $rsEnvio = DB::getInstance()->prepare($query_rsEnvio);
    $rsEnvio->execute();    
    $row_rsEnvio = $rsEnvio->fetchAll(PDO::FETCH_ASSOC);
    $totalRows_rsEnvio = $rsEnvio->rowCount();
  }

  $query_rsNewslleter = "SELECT imagem_news FROM homepage".$extensao;
  $rsNewslleter = DB::getInstance()->prepare($query_rsNewslleter);
  $rsNewslleter->execute();    
  $row_rsNewslleter = $rsNewslleter->fetch(PDO::FETCH_ASSOC);
  $totalRows_rsNewslleter = $rsNewslleter->rowCount();
  
  ?>
  <footer class="div_100 footer">
    <section class="div_100 section-info">
      <div class="row collapse full">
        <div class="column small-12 medium-6" style="z-index: 1;">
          <div class="div_100 h-100 newsImage has_bg lazy" data-src="<?php echo 'homepage/'.$row_rsNewslleter['imagem_news']; ?>">
           <?php echo getFill('homepage',2); ?>
           <div class="sofas"><?php echo file_get_contents(ROOTPATH."imgs/elem/sofas.svg"); ?></div>
           <div class="salas"><?php echo file_get_contents(ROOTPATH."imgs/elem/salas.svg"); ?></div>
           <div class="moveis show-for-medium"><?php echo file_get_contents(ROOTPATH."imgs/elem/moveis.svg"); ?></div>
         </div>
       </div>
       <div class="column small-12 medium-6" style="z-index: 2;">
        <form method="post" id="form_subscreve" name="form_subscreve" autocomplete="off" action="javascript:subs_news('form_subscreve');" onSubmit="return validaForm('form_subscreve')" novalidate class="news_cont" data-error="<?php echo $Recursos->Resources["news_subs"]; ?>">
          <h3 class="subtitulos uppercase"><?php echo $Recursos->Resources['news_tit']; ?></h3>
          <div class="inpt_holder full">
            <input class="inpt" required type="email" class="inpt" id="email_subs" name="email_subs" placeholder="<?php echo $Recursos->Resources["news_subs"]; ?>"/>
          </div>
          <div class="inpt_holder simple full no_marg">
           <div class="inpt_checkbox">
            <input type="checkbox" required name="termos_news" id="termos_news" value="1" />
            <label for="termos_news"><?php echo $Recursos->Resources["aceito_termos_curto"]; ?></label>
          </div>
        </div>
        <button class="button" aria-label="Submit form" role="button" type="submit"><?php echo $Recursos->Resources["enviar"]; ?></button>
        <div class="anular_subs"><a href="news_remover.php"><?php echo $Recursos->Resources["anular_subscricao"]; ?></a></div>
      </form>
    </div>
  </div>
</section>
<section class="div_100 links-section">
  <div class="row">
    <?php if(!empty($GLOBALS['divs_categorias'])) { ?>
      <div class="column small-6 medium-2">
        <h3 class="list_subtit"><?php echo $Recursos->Resources["loja"]; ?></h3>
        <ul>
          <?php foreach($GLOBALS['divs_categorias'] as $cats) {
            $subs_cats = $cats['subs'];
            if($cats['info']) {
              $cats = $cats['info'];
            }
            ?>
            <li><a class="list_txt" href="<?php echo ROOTPATH_HTTP_LANG.$cats['url']; ?>"><?php echo $cats["nome"]; ?></a></li>
          <?php } ?>
        </ul>
      </div>
    <?php } ?>
    <div class="column small-6 medium-2">
      <!-- Se nao quiseres o dropdown: css -> H3 colocar: pointer-events: none; -->
      <h3 class="list_subtit"><?php echo $Recursos->Resources["title-footer"]; ?></h3>
      <ul>
        <li><a class="list_txt" href="<?php echo get_meta_link(2); ?>"><?php echo $Recursos->Resources["contactos"]; ?></a></li>
        <li><a class="list_txt" href="<?php echo $pagSobre['url']; ?>"><?php echo $pagSobre["nome"]; ?></a></li>
        <li><a class="list_txt" href="<?php echo get_meta_link(5); ?>"><?php echo $Recursos->Resources["faqs"]; ?></a></li>
      </ul>
    </div>

    <div class="column small-6 medium-2">
      <h3 class="list_subtit"><?php echo $Recursos->Resources['title-footer-2']; ?></h3>
      <ul>
        <li><a class="list_txt" href="<?php echo ROOTPATH_HTTP_LANG.$pagCookies['url']; ?>"><?php echo $pagCookies['nome']; ?></a></li>
        <li><a class="list_txt" href="<?php echo ROOTPATH_HTTP_LANG.$pagTermos['url']; ?>"><?php echo $pagTermos['nome']; ?></a></li>
        <li><a class="list_txt" href="<?php echo ROOTPATH_HTTP_LANG.$pagPolitica['url']; ?>"><?php echo $pagPolitica['nome']; ?></a></li>
        <li><a class="list_txt" href="<?php echo ROOTPATH_HTTP_LANG.$pagRal['url']; ?>"><?php echo $pagRal['nome']; ?></a></li>
      </ul>
    </div>
    <div class="column small-6 medium-2">
        <h3 class="list_subtit"><?php echo $Recursos->Resources['siga']; ?></h3>
        <div><!--
          --><?php foreach($GLOBALS['divs_redes'] as $redes) { ?><!--
            --><a class="share-<?php echo strtolower($redes['nome']); ?>" href="<?php echo $redes['link']; ?>" target="_blank"></a><!--
          --><?php } ?>
        </div>
    </div>
    <div class="column small-12 medium-4 ">
      <div class="pagamento">
        <?php if($totalRows_rsPagamento > 0) { ?>
          <div class="div_100">
            <?php foreach($row_rsPagamento as $pagamento) { 
              if($pagamento['imagem2'] && file_exists(ROOTPATH."imgs/carrinho/".$pagamento['imagem2'])) { ?>
                <div class="divs_pagamentos">
                  <div class="has_bg contain lazy" data-src="carrinho/<?php echo $pagamento['imagem2']; ?>" alt="<?php echo $pagamento['nome']; ?>" title="<?php echo $pagamento['nome']; ?>"></div>
                </div><?php } 
            } ?>
          </div>
        <?php } ?>
        <?php if($totalRows_rsEnvio > 0) { ?>
          <div class="div_100">
            <?php foreach($row_rsEnvio as $envio) { 
              if($envio['imagem2'] && file_exists(ROOTPATH."imgs/carrinho/".$envio['imagem2'])) { ?>
                <div class="divs_pagamentos">
                  <div class="has_bg contain lazy" data-src="carrinho/<?php echo $envio['imagem2']; ?>" alt="<?php echo $envio['nome']; ?>" title="<?php echo $envio['nome']; ?>"></div>
                </div>
              <?php } 
            } ?>
          </div>
        <?php } ?>
        </div>
    </div>
  </div>
</section>
<section class="div_100 section-direitos">
  <div class="row align-middle">
    <div class="column small-12 medium-2 order-2 medium-order-1">
      <div class="div_100">
        <a class="reclamacoes" href="https://www.livroreclamacoes.pt/inicio" target="_blank">
          <?php echo file_get_contents(ROOTPATH."imgs/elem/reclamacoes.svg"); ?>
        </a>
      </div>
    </div>
    <div class="column medium-expand order-1 medium-order-2 text-center">
      <h6 class="list_subtit"><?php echo $Recursos->Resources['direitos']; ?></h6>
    </div>
    <div class="column medium-2 text-right show-for-medium medium-order-3">
      <a class="icon-netgocio" href="http://netgocio.pt" target="_blank"></a>
    </div>
  </div>
</section>
</footer>
<?php } else { ?>
  <div class="row2">
    <div class="div_table_cell">
      <div class="row content login_ft text-center">
        <div class="column small-12 xxsmall-expand xxsmall-order-2 xxsmall-text-right">
          <a class="list_txt" target="_blank" href="<?php echo ROOTPATH_HTTP_LANG.$pagTermos['url']; ?>"><?php echo $pagTermos['nome']; ?></a><!-- 
          --><span class="list_txt"> | </span><!-- 
          --><a class="list_txt" target="_blank" href="<?php echo ROOTPATH_HTTP_LANG.$pagPolitica['url']; ?>"><?php echo $pagPolitica['nome']; ?></a>
        </div>
        <div class="column small-12 xxsmall-shrink xxsmall-order-1 xxsmall-text-left">
          <h6 class="list_txt"><?php echo $Recursos->Resources['direitos']; ?></h6>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<div id="voltar_acima" class="bottom-actions voltar_topo" onclick="goTo('body')">
  <?php echo file_get_contents(ROOTPATH."imgs/elem/goTop.svg"); ?>
</div>

<?php if(file_exists(ROOTPATH.'includes/testemunhos_modal.php')) {
  include_once(ROOTPATH.'includes/testemunhos_modal.php'); 
} ?>

<?php if(ECOMMERCE == 1) { ?>
  <div id="cart-trigger" class="bottom-actions cart-trigger icon-cart">
    <span class="count"></span>
    <button class="close-button white" ntgmodal-close aria-label="Close reveal" role="button" type="button">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php if($menu_sel != "area_reservada" && tableExists(DB::getInstance(), 'lista_desejo')) {
    if($row_rsCliente != 0) {
      $id_cliente = $row_rsCliente['id'];
      $where_list = "lista.cliente = '$id_cliente'";
    } 
    else {
      $wish_session = $_COOKIE[WISHLIST_SESSION];
      $ses_id_old = strtotime(date("YmdHis", strtotime("-5 days"))); //5 dias atr�s
      
      if($wish_session == "" || $wish_session <= $ses_id_old) {
        $ses_id=strtotime(date("YmdHis", time()));
        
        $insertSQL = "DELETE FROM lista_desejo WHERE cliente = 0 AND session < '$ses_id_old'";
        $rsInsertSQL = DB::getInstance()->prepare($insertSQL);
        $rsInsertSQL->execute();
        
        $timeout = 3600*24*5; //5 dias
        setcookie(WISHLIST_SESSION, $ses_id, time()+$timeout, "/", "", $cookie_secure, true);
        $wish_session = $ses_id;
      }

      $where_list = "lista.session = '$wish_session'";
    }
      
    $totalRows_rsFavorito = 0;
    if($where_list) {
      $query_rsFavorito = "SELECT lista.id FROM lista_desejo AS lista LEFT JOIN l_pecas".$extensao." AS pecas ON lista.produto = pecas.id WHERE ".$where_list." AND pecas.visivel = 1 GROUP BY pecas.id ORDER BY pecas.ordem ASC LIMIT 1";
      $rsFavorito = DB::getInstance()->prepare($query_rsFavorito);
      $rsFavorito->execute();
      $row_rsFavorito = $rsFavorito->fetch(PDO::FETCH_ASSOC);
      $totalRows_rsFavorito = $rsFavorito->rowCount();
    }
    ?>
    <a href="area-reservada-favoritos.php" id="wish-trigger" class="bottom-actions wish-trigger icon-favorite show-for-medium <?php if($totalRows_rsFavorito>0) echo " visible";?>"></a>
  <?php } ?>
<?php } ?>

<div id="outdated">
  <div class="div_100" style="height:100%">
    <div class="div_table_cell">
    
    </div>
  </div>
</div>

<?php if($popup == 1 && $ishome == 1) { ?>
  <div class="div_100 popup_container">
    <div class="div_100" style="height: 100%">
      <div class="div_table_cell">
        <a href="<?php echo $row_rsPopUp['link_popup']; ?>" target="_blank">
          <div class="popup_close"></div>
          <img src="<?php echo ROOTPATH_HTTP; ?>imgs/popup/<?php echo $row_rsPopUp['imagem_popup']; ?>" width="100%" style="max-width:1000px; margin:auto" />
        </a>
      </div>
    </div>
  </div>  
<?php } ?>

<?php DB::close(); ?>

<?php if($no_scripts != 1) {
  include_once('footer_scripts.php'); 
} ?>