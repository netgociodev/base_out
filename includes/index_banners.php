<?php if(!empty($GLOBALS['divs_banners'])) { ?>
  <div class="div_100 banners fullscreen">
    <div class="div_100 h-100">      
      <div class="div_table_cell">
        <div id="banners1" class="slick-banners full_height">
          <?php foreach($GLOBALS['divs_banners'] as $row) { ?>
            <?php if(($row['imagem1'] && file_exists(ROOTPATH.'imgs/banners/'.$row['imagem1'])) || $row['video'] || $row['id_video'] > 0) { 
              $color = "";
              $color2 = "";
              if($row['cor1']) {
                $color = $row['cor1'];
              }
              if($row['cor2']) {
                $color2 = $row['cor2'];
              }

              $mask = "";
              if($row['mascara1'] == 1) {
                $mask .= " has_mask"; 
              }
              if($row['mascara2'] == 1) {
                $mask .= " mobile_has_mask"; 
              }

              $alignH = "center"; 
              $alignV = "center";
              $alignH2 = "center"; 
              $alignV2 = "center";

              if($row['align_h1']) {
                $alignH = $row['align_h1']; 
              }
              if($row['align_v1']) {
                $alignV = $row['align_v1'];
              }

              if($row['align_h2']) {
                $alignH2 = $row['align_h2']; 
              }
              if($row['align_v2']) {
                $alignV2 = $row['align_v2'];
              }

              $text_align ="";
              $text_align2 = "";
              if($row['text_alignv']) {
                $text_align .= " align-".$row['text_alignv']; 
              }
              if($row['text_alignh']) {
                $text_align .= " align-".$row['text_alignh'];
                $text_align2 .= " medium-text-".$row['text_alignh'];
              }

              $align = "align_".$alignH."_".$alignV;
              $align2 = "align_".$alignH2."_".$alignV2;

              $img_banners = "";
              if($row['imagem2'] && file_exists(ROOTPATH.'imgs/banners/'.$row['imagem2'])) {
                $img_banners = ROOTPATH_HTTP."imgs/banners/".$row['imagem2'];
              }
              else if($row['imagem1'] && file_exists(ROOTPATH.'imgs/banners/'.$row['imagem1'])) {
                $img_banners = ROOTPATH_HTTP."imgs/banners/".$row['imagem1'];
              }

              $thumb = ROOTPATH_HTTP."imgs/elem/video.png";
              if($row['imagem3'] && file_exists(ROOTPATH."imgs/banners/".$row['imagem3'])) {
                $thumb = ROOTPATH_HTTP."imgs/banners/".$row['imagem3'];
              }
              ?>
              <?php if(($row['imagem1'] && file_exists(ROOTPATH.'imgs/banners/'.$row['imagem1']) ) ) { ?>
              <?php 
                $is_vid1 = strrchr($row['imagem1'], '.');
              ?>

              <?php if ($is_vid1 != '.mp4'){  //IMAGEM DESKTOP E MOBILE ?>
                <div class="banners_slide has_bg<?php echo $mask; ?>" data-thumb="<?php echo $thumb; ?>" bg-srcset="<?php echo $img_banners; ?> 950w <?php echo $align2; ?>, <?php echo ROOTPATH_HTTP; ?>imgs/banners/<?php echo $row['imagem1']; ?> <?php echo $align; ?>" style="position:relative;" id="slide<?php echo $row['id']; ?>">

             <?php } else if($is_vid1 == ".mp4"){ //O VIDEO ENTRA NESTE ?>
                <div class="banners_video has_bg <?php echo $mask; ?>" style="position:relative;" id="slide<?php echo $row['id']; ?>">
                  <div class="div_100 wrapper_video_player">

                    <?php if ($row['imagem1'] != $row['imagem2']){ //O VIDEO MOBILE DIFERENTE DO DESKTOP
                      $is_vid2 = strrchr($row['imagem2'], '.');
                      if($is_vid2 == ".mp4"){ ?>
                        <div class="video_cont hide-for-medium">
                          <video id="video1" class="video_player" autobuffer="autobuffer" loop muted="muted"> <?php /* controls */ ?>
                          <source src="<?php echo ROOTPATH_HTTP."imgs/banners/".$row['imagem2']; ?>" type="video/mp4">
                          </video>
                        </div> 
                     <?php } else{ //O IMAGEM MOBILE COM VIDEO DESKTOP ?>
                        <div class="image_cont has_bg hide-for-medium" style="background-image: url(<?php echo ROOTPATH_HTTP."imgs/banners/".$row['imagem2'];?>); background-position-x:<?php echo $alignH2; ?>; background-position-y:<?php echo $alignV2; ?>"></div>
                      <?php }
                     } ?>
                     
                    <div class="video_cont <?php if ($row['imagem1'] != $row['imagem2']){ echo 'show-for-medium'; }?>">
                      <video id="video1" class="video_player" autobuffer="autobuffer" loop muted="muted"> <?php /* controls */ ?>
                      <source src="<?php echo ROOTPATH_HTTP."imgs/banners/".$row['imagem1']; ?>" type="video/mp4">
                      </video>
                    </div> 
                  </div> 
                <?php } ?>


                  <!--IF full nao precisa ter isto -->
                  <?php /*<div class="div_100 show-for-medium">
                    <?php echo getFill('banners'); ?>
                  </div>
                  <div class="div_100 hide-for-medium">
                    <?php echo getFill('banners', 2); ?>
                  </div>*/ ?>
                  <div class="banner_cont text-right">
                    <div class="row <?php echo $text_align.$text_align2; ?> h-100 text-center">
                      <div class="column small-12">                                   
                        <div class="banner_content">
                          <?php if($row['titulo']) { ?>
                            <h1 class="titulos show-for-medium"<?php if($color) echo ' style="color:'.$color.'"';?>><?php echo $row['titulo']; ?></h1>
                            <h1 class="titulos hide-for-medium"<?php if($color2) echo ' style="color:'.$color2.'"';?>><?php echo $row['titulo']; ?></h1>
                          <?php } ?>
                          <?php if($row['subtitulo']) { ?>
                            <h2 class="show-for-medium"<?php if($color) echo ' style="color:'.$color.'"';?>><?php echo str_text($row['subtitulo'], 124); ?></h2>
                            <h2 class="hide-for-medium"<?php if($color2) echo ' style="color:'.$color2.'"';?>><?php echo str_text($row['subtitulo'], 124); ?></h2>
                          <?php } ?>
                          <?php if($row['link']) { ?>
                            <?php if($row['target'] != "_video") { ?>
                              <?php if($row['texto_link'] && $row['texto_link'] != "") { ?>
                                <?php echo text_link($row['link'], $row['target'], $row['texto_link'], "button ".$row['link_class']); ?>
                              <?php } ?>
                            <?php } else { ?>
                              <a href="javascript:;" onclick="carregaVideoBanner('<?php echo $row['link']; ?>');"><?php echo $texto_link; ?></a>
                            <?php } ?>
                          <?php } ?>            
                          <?php if($row['campanha'] == 1) { ?>
                            <p class="list_txt text-center medium-text-left"><?php echo campanhaValida($row['datai'], $row['dataf']); ?></p>
                          <?php } ?>                            
                        </div>
                      </div>
                    </div>
                    <?php if($row['link']) { ?>
                     <a href="<?php echo $row['link']; ?>" target="<?php echo $row['target']; ?>" class="linker"></a>
                    <?php } ?>
                  </div> 

                </div>
              <?php } else if($row['video']) {
                $video = $row['video'];
                $class = "";

                if(strstr($video, "youtube") || strstr($video, "youtu.be")) {
                  $class = " youtube full";
                }
                else if(strstr($video, "vimeo")) {
                  $class = " vimeo full";
                }
                else{
                  $class = "iframe";
                }
                
                ?>
                <div data-thumb="<?php echo $thumb; ?>" style="position:relative;">
                  <div class="div_100 show-for-medium">
                    <?php echo getFill('banners'); ?>
                  </div>
                  <div class="div_100 hide-for-medium">
                    <?php echo getFill('banners', 2); ?>
                  </div>
                  <?php if($class == "iframe") { ?>
                    <iframe src="<?php echo $video; ?>" allowfullscreen width="854" height="480" frameborder="0"></iframe>
                  <?php } else { ?>
                    <div class="video_frame absolute<?php echo $class; ?>" data-vid="<?php echo $video; ?>"></div>
                  <?php } ?>  
                </div>                          
              <?php } ?>
            <?php } ?> 
          <?php } ?> 
        </div>
      </div>
    </div>
    <div id="modalFull" ntgmodal ntgmodal-size="large">
      <div ntgmodal-content>
        <button class="close-button" data-close aria-label="Close reveal" type="button" onclick="carregaVideoBanner('');">
          <span aria-hidden="true">&times;</span>
        </button>                            
        <div id="video_container"></div>
      </div>
    </div>
  </div>   
<?php } ?>