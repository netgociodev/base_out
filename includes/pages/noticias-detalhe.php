<?php include_once('pages_head.php');
geraSessions('noticias');

$noticia = $_GET['id'];

$row_rsNoticias = $GLOBALS['divs_noticias'][$noticia];
if($GLOBALS['divs_noticias'][$noticia]['info']) {
    $row_rsSubs = $GLOBALS['divs_noticias'][$noticia]['subs'];
    $row_rsNoticias = $GLOBALS['divs_noticias'][$noticia]['info'];
}

$text_align = "";
if($row_rsNoticias['text_align']){
    $text_align = " ".$row_rsNoticias['text_align'];
}

//procura produto anterior e seguinte
$query_rsTotal = "SELECT * FROM noticias".$extensao." WHERE visivel = '1' ORDER BY ordem ASC";
$rsTotal = DB::getInstance()->prepare($query_rsTotal);
$rsTotal->execute();
$totalRows_rsTotal = $rsTotal->rowCount();
DB::close();

$prod_ant = "";
$prod_ant_id = "";
$prod_seg = "";
$prod_seg_id = "";
$encontrado = 0;
$conta_reg = 0;

if($totalRows_rsTotal > 1) {
    while($row_rsTotal = $rsTotal->fetch()){
        
        $registo_actual++;
        
        if($encontrado == 1){
            $prod_seg = $row_rsTotal['url'];
            $prod_seg_id = $row_rsTotal['id'];
            break;          
        }
        
        if($row_rsTotal['id'] != $noticia && $encontrado == 0) {
            $prod_ant = $row_rsTotal['url'];
            $prod_ant_id = $row_rsTotal['id'];
        } else if($row_rsTotal['id'] == $noticia) {
            $encontrado = 1;            
        }
    
    }
}else{
    $registo_actual++;
}

$query_rsMetatags = "SELECT title, description, keywords, url FROM noticias".$extensao." WHERE id = :id";
$meta_id = $noticia;

$menu_sel="noticias";
?>

<main class="div_100 noticias-detail">
    <a class="close-button" data-remote="true" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/noticias.php" data-pagetrans="close-noticias-detail">&times;</a>
    <section class="div_100 noticias_detalhe all_stick">
        <article class="row collapse align-center">
            <?php if(!empty($row_rsSubs)){ ?>  
            <header class="small-12 medium-5 medium-order-2 column" role="banner">
                <div class="div_100 img">
                    <div class="slick-cont" style="display:block;">
                        <?php foreach($row_rsSubs as $row) { ?>
                            <?php if($row['imagem1'] && file_exists(ROOTPATH.'imgs/noticias/'.$row['imagem1'])){ ?>
                            <div class="slide">
                                <div class="div_100 has_bg lazy" data-src="noticias/<?php echo $row['imagem1']; ?>">
                                    <?php echo getFill('noticias', '', ROOTPATH."imgs/noticias/".$row['imagem1']); ?>   
                                </div>
                            </div>
                            <?php } ?> 
                        <?php } ?> 
                    </div>
                </div>
            </header>
            <?php } ?> 
            <main class="small-12 medium-7 medium-order-1 column text-left">
                <?php if(empty($row_rsSubs) && $row_rsNoticias['imagem1'] && file_exists(ROOTPATH.'imgs/noticias/'.$row_rsNoticias['imagem1'])){ ?>
                    <picture class="div_100">
                        <div class="div_100 has_bg lazy contain " data-src="noticias/<?php echo $row_rsNoticias['imagem1']; ?>">
                            <?php echo getFill('noticias'); ?>  
                        </div>                    
                    </picture>
                <?php } ?> 
                <div class="div_100 info to_sticky" id="noticia_info" style="left: auto;">
                    <div class="div_100 navigation">
                        <div class="svg-wrap hidden">
                            <svg width="64" height="64" viewBox="0 0 64 64">
                                <path id="arrow-left" d="M44.797 17.28l0.003 29.44-25.6-14.72z" />
                            </svg>
                            <svg width="64" height="64" viewBox="0 0 64 64">
                                <path id="arrow-right" d="M19.203 17.28l-0.003 29.44 25.6-14.72z" />
                            </svg>
                        </div>
                        <div class="row collapse">
                            <div class="column">
                                <?php if($prod_ant){ ?>
                                <a class="prev" href="<?php echo $prod_ant; ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/noticias-detalhe.php" data-ajaxTax="<?php echo $prod_ant_id; ?>" data-remote="true" data-pagetrans="noticias-detail" data-detail="1"> 
                                    <span class="icon-wrap"><svg class="icon" width="20" height="20" viewBox="0 0 64 64"><use xlink:href="#arrow-left"></use></svg></span>
                                    <div><span class="list_subtit"><?php echo $Recursos->Resources["anterior"]; ?></span></div>
                                </a>
                                <?php } ?>
                            </div>
                            <?php if($prod_seg){ ?>
                            <div class="column text-right">
                                <a class="next" href="<?php echo $prod_seg; ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/noticias-detalhe.php" data-ajaxTax="<?php echo $prod_seg_id; ?>" data-remote="true" data-pagetrans="noticias-detail" data-detail="1">
                                    <span class="icon-wrap"><svg class="icon" width="20" height="20" viewBox="0 0 64 64"><use xlink:href="#arrow-right"></use></svg></span>
                                    <div><span class="list_subtit"><?php echo $Recursos->Resources["seguinte"]; ?></span></div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="div_100 container text-center">
                        <h1 class="subtitulos<?php echo $text_align; ?>"><?php echo $row_rsNoticias['nome']; ?></h1>
                        <div class="desc textos text-left"><?php echo $row_rsNoticias['descricao']; ?></div>
                        <?php if($row_rsNoticias['ficheiro'] && file_exists(ROOTPATH."imgs/noticias/".$row_rsNoticias['ficheiro'])){ ?>
                        <div class="div_100 text-center" style="margin-top: 20px">
                            <a class="button" href="<?php echo ROOTPATH_HTTP."imgs/noticias/".$row_rsNoticias['ficheiro']; ?>" target="_blank">Download</a>
                        </div>
                        <?php } ?>
                        <div class="share">
                            <?php
                            $sharePos = "top";
                            $shareClass = "shareNormal"; //shareInvert
                            $shareNome = urlencode(utf8_encode($row_rsNoticias["nome"]));
                            $shareDesc = urlencode(str_replace(utf8_encode('"'), "'", $row_rsNoticias["resumo"]));
                            $shareUrl = ROOTPATH_HTTP.$row_rsNoticias["url"];
                            if($countLang>1) $shareUrl = ROOTPATH_HTTP.$lang."/".$row_rsNoticias["url"];
                            $shareImg = ROOTPATH_HTTP."/imgs/noticias/".$row_rsNoticias["imagem1"];
                            include_once(ROOTPATH.'includes/share-list.php');
                            ?>
                        </div>
                    </div>
                </div>
            </main>
        </article>
    </section>    
</main>

<?php include_once('pages_footer.php'); ?>